let Block = Quill.import('blots/block');
let BlockEmbed = Quill.import('blots/block/embed');
let Inline = Quill.import('blots/inline');
let Parchment = Quill.import('parchment');

class BlockQuoteBlot extends Block {
}

BlockQuoteBlot.blotName = 'blockquote';
BlockQuoteBlot.className = 'blockquote';
BlockQuoteBlot.tagName = 'blockquote';

class BoldBlot extends Inline { }
BoldBlot.blotName = 'bold';
BoldBlot.tagName = 'strong';

class DividerBlot extends BlockEmbed { 
    static create(value) {
        let node = super.create();
        return node;
    }
}
DividerBlot.blotName = 'divider';
DividerBlot.tagName = 'hr'

class HeaderBlot extends Block {
    static formats(node) {
        return HeaderBlot.tagName.indexOf(node.tagName) + 1;
    }
}

HeaderBlot.blotName = 'header';
HeaderBlot.tagName = ['H1', 'H2'];

class ItalicBlot extends Inline { }
ItalicBlot.blotName = 'foo';
ItalicBlot.tagName = 'em';

class ImageBlot extends BlockEmbed {
    static create(value) {
        let node = super.create();
        node.setAttribute('alt', value.alt);
        node.setAttribute('src', value.url);
        node.classList.add('mx-auto');
        node.classList.add('d-block');

        let t = this;

        //node.addEventListener('click', this.select(t, node));

        return node;
    }

    // static select(t, node) {
    //     return function(e) {
    //         let blot = Quill.find(node);
    //         node.classList.add('active');
    //         console.debug("ImageBlot Click", blot);

    //         e.stopImmediatePropagation();

    //         node.closest('.ql-editor').addEventListener('click', t.deselect(node));
    //     }
    // }

    // // https://triangle717.wordpress.com/2015/12/14/js-avoid-duplicate-listeners/
    // static deselect(node) {
    //     return function() {
    //         node.classList.remove('active');
    //     }
    // }

    static value(node) {
        return {
            alt: node.getAttribute('alt'),
            url: node.getAttribute('src'),
        }
    }
}

ImageBlot.blotName = 'image';
ImageBlot.tagName = 'img';

class LinkBlot extends Inline {
    static create(value) {
        let node = super.create();
        node.setAttribute('href', value);
        node.setAttribute('target', '_blank');
        return node;
    }

    static formats(node) {
        return node.getAttribute('href');
    }
}

LinkBlot.blotName = 'link';
LinkBlot.tagName = 'a';

class VideoBlot extends BlockEmbed {
    static create(url) {
        let node = super.create();

        node.setAttribute('src', url);
        node.setAttribute('frameborder', '0');
        node.setAttribute('allowfullscreen', true);

        return node;
    }

    static formats(node) {
        // We still need to report unregistered embed formats
        let format = {};
        if (node.hasAttribute('height')) {
          format.height = node.getAttribute('height');
        }
        if (node.hasAttribute('width')) {
          format.width = node.getAttribute('width');
        }
        return format;
    }

    static value(node) {
        return node.getAttribute('src');
    }
    
    format(name, value) {
        // Handle unregistered embed formats
        if (name === 'height' || name === 'width') {
          if (value) {
            this.domNode.setAttribute(name, value);
          } else {
            this.domNode.removeAttribute(name, value);
          }
        } else {
          super.format(name, value);
        }
    }
}

VideoBlot.blotName = 'video';
VideoBlot.tagName = 'iframe';

/* Quill Setup */

Quill.register(BlockQuoteBlot);
Quill.register(BoldBlot);
Quill.register(DividerBlot);
Quill.register(HeaderBlot);
Quill.register(ImageBlot);
Quill.register(ItalicBlot);
Quill.register(LinkBlot);
Quill.register(VideoBlot);

let quill = new Quill('#editor-container');

quill.insertText(0, 'Test', { bold: true });
quill.formatText(0, 4, 'foo', true);

// Selectable image
// https://github.com/quilljs/quill/issues/1055#issuecomment-258944398
quill.root.addEventListener('click', e => {
    let image = Parchment.find(e.target);
  
    if (image instanceof ImageBlot) {
        quill.setSelection(image.offset(quill.scroll), 1, 'user');
    }
});

$('#blockquote-button').click(function() {
    quill.format('blockquote', true);
});

$('#bold-button').click(function() {
    quill.format('bold', true);
});

$('#divider-button').click(function() {
    let range = quill.getSelection(true);
    quill.insertText(range.index, '\n', Quill.sources.USER);
    quill.insertEmbed(range.index + 1, 'divider', true, Quill.sources.USER);
    quill.setSelection(range.index + 2, Quill.sources.SILENT);
});

$('#header-1-button').click(function() {
    quill.format('header', 1);
});

$('#header-2-button').click(function() {
    quill.format('header', 2);
});

$('#image-button').click(function() {
    let range = quill.getSelection(true);
    quill.insertText(range.index, '\n', Quill.sources.USER);
    quill.insertEmbed(range.index + 1, 'image', {
      alt: 'Quill Cloud',
      url: 'https://quilljs.com/0.20/assets/images/cloud.png'
    }, Quill.sources.USER);
    quill.setSelection(range.index + 2, Quill.sources.SILENT);
});

$('#italic-button').click(function() {
    quill.format('italic', true);
});

$('#link-button').click(function() {
    let value = prompt('Enter link URL');
    quill.format('link', value);
});

$('#video-button').click(function() {
    let range = quill.getSelection(true);
    quill.insertText(range.index, '\n', Quill.sources.USER);
    let url = 'https://www.youtube.com/embed/QHH3iSeDBLo?showinfo=0';
    quill.insertEmbed(range.index + 1, 'video', url, Quill.sources.USER);
    quill.formatText(range.index + 1, 1, { height: '170', width: '400' });
    quill.setSelection(range.index + 2, Quill.sources.SILENT);
});

