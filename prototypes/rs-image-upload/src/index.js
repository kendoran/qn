import RemoteStorage from 'remotestoragejs';
import './index.css';

var ImagesModule = { name: 'images', builder: function(privateClient, publicClient) {
    return {
      exports: {
       addHtml: function() {
           privateClient.storeFile('text/html', 'index.html', '<h1>Hello World!</h1>')
                        .then(() => { console.log("Upload done") });
       },
       storeFile: function(mimeType, fileName, body) {
           privateClient.storeFile(mimeType, fileName, body)
                        .then(() => { console.log("addBinary done") });
       },
       getFile: function(fileName) {
            return privateClient.getFile(fileName);
       }
      }
    }
  }};

const remoteStorage = new RemoteStorage({logging: true, modules: [ ImagesModule ]});

remoteStorage.access.claim('images', 'rw');
remoteStorage.caching.enable('/images/')

remoteStorage.images.addHtml();

document.querySelector('form#upload button[type=submit]').onclick = function(e) {
    console.log('flick');

    e.preventDefault();

    var input = document.querySelector('form#upload input[type=file]');
    var file = input.files[0];
    console.log(file);
    var fileReader = new FileReader();
    
    fileReader.onload = function (e) {
        remoteStorage.images.storeFile(file.type, file.name, fileReader.result);
    };

    fileReader.readAsArrayBuffer(file);
}

remoteStorage.images.getFile('Image from iOS.jpg').then(file => {
    var blob = new Blob([file.data], { type: file.mimeType });
    console.log(file.data, file.mimeType);
    var targetElement = document.getElementById('im');
    targetElement.src = window.URL.createObjectURL(blob);
});
    