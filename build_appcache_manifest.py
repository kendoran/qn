"""Builder"""
import glob
import time
import os

if __name__ == "__main__":
    base_dir = os.getcwd()
    output_file = os.path.join(base_dir, 'deploy\\build\\manifest.appcache')

    js_dir = os.path.join(base_dir, 'build\\static\\js\\main.*.js')
    js_glob = glob.glob(js_dir)[0]
    js_main_filename = os.path.basename(js_glob)

    css_dir = os.path.join(base_dir, 'build\\static\\css\\main.*.css')
    css_glob = glob.glob(css_dir)[0]
    css_main_filename = os.path.basename(css_glob)

    header = "CACHE MANIFEST\n"
    header = header + "#" + str(time.time()) + "\n"

    header = header + 'static/js/' + js_main_filename + "\n"
    header = header + 'static/css/' + css_main_filename + "\n"

    manifest_appcache_content = header + """static/js/ckeditor.js
static/js/remotestorage.js
static/css/fontawesome.css
static/webfonts/fa-brands-400.ttf
static/webfonts/fa-brands-400.woff
static/webfonts/fa-brands-400.woff2
static/webfonts/fa-regular-400.eot
static/webfonts/fa-regular-400.svg
static/webfonts/fa-regular-400.ttf
static/webfonts/fa-regular-400.woff
static/webfonts/fa-regular-400.woff2
static/webfonts/fa-solid-900.eot
static/webfonts/fa-solid-900.svg
static/webfonts/fa-solid-900.ttf
static/webfonts/fa-solid-900.woff
static/webfonts/fa-solid-900.woff2

NETWORK:
*

FALLBACK:
/static/webfonts/fa-brands-400.eot static/webfonts/fa-brands-400.eot
/static/webfonts/fa-brands-400.svg static/webfonts/fa-brands-400.svg
/static/webfonts/fa-brands-400.ttf static/webfonts/fa-brands-400.ttf
/static/webfonts/fa-brands-400.woff static/webfonts/fa-brands-400.woff
/static/webfonts/fa-brands-400.woff2 static/webfonts/fa-brands-400.woff2
/static/webfonts/fa-regular-400.eot static/webfonts/fa-regular-400.eot
/static/webfonts/fa-regular-400.svg static/webfonts/fa-regular-400.svg
/static/webfonts/fa-regular-400.ttf static/webfonts/fa-regular-400.ttf
/static/webfonts/fa-regular-400.woff static/webfonts/fa-regular-400.woff
/static/webfonts/fa-regular-400.woff2 static/webfonts/fa-regular-400.woff2
/static/webfonts/fa-solid-900.eot static/webfonts/fa-solid-900.eot
/static/webfonts/fa-solid-900.svg static/webfonts/fa-solid-900.svg
/static/webfonts/fa-solid-900.ttf static/webfonts/fa-solid-900.ttf
/static/webfonts/fa-solid-900.woff static/webfonts/fa-solid-900.woff
/static/webfonts/fa-solid-900.woff2 static/webfonts/fa-solid-900.woff2
"""

    with open(output_file, mode='w') as f:
        f.write(manifest_appcache_content)
