import Fuse from 'fuse.js'
import { parse, stringify } from './externals/QueryString'
// import { stringify } from 'querystring';

export function isEmptyObject(obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object
}

//=====================================
// Query string
//=====================================

export const MODE = {
  EDIT_INLINE: 'edit_inline',
  EDIT_MOBILE: 'edit_mobile',
  LIST: 'list',
  SEARCH: 'search',
};

export const PAGEQUERY = {
  TODAY: 'today',
  AGENDA: 'agenda',
  ALL: 'all',
};

export const DEVICE = {
  EXTRA_LARGE: 1200, // Extra large devices (large desktops, 1200px and up)
  LARGE: 992, // Large devices (desktops, 992px and up)
  MEDIUM: 768, // Medium devices (tablets, 768px and up)
  SMALL: 576, // Small devices (landscape phones, 576px and up)
};

export function getWindowWidth() {
  return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}

export function getCurrentMode(location) {
  if (location && parse(location.search).note) {
    return getWindowWidth() >= DEVICE.LARGE ? MODE.EDIT_INLINE : MODE.EDIT_MOBILE;
  } else if (location && location.search.indexOf('q=') > -1) {
    return MODE.SEARCH;
  } else {
    return MODE.LIST;
  }
}

export function isNewNote(location) {
  return parse(location.search)['is_new'] === "true";
}

export function getNoteIdFromQueryString(location) {
  return parse(location.search).note;
}

export function buildQueryStringForNote(location, note) {
  const queryProps = parse(location.search);
  queryProps.note = note.id;
  return "?" + stringify(queryProps);
}

export function buildQueryStringForNoteId(location, noteId, isNew) {
  const queryProps = parse(location.search);
  queryProps.note = noteId;

  if (isNew) {
    queryProps['is_new'] = true;
  }

  return "?" + stringify(queryProps);
}

export function buildQueryStringForCloseNote(location) {
  let queryProps = parse(location.search)
  delete queryProps.note;
  return "?" + stringify(queryProps);
}

export function buildQueryStringWithoutNoteId(location) {
  const queryProps = parse(location.search);
  delete queryProps.note;
  return "?" + stringify(queryProps);
}

export function getSearchQueryFromQueryString(location) {
  return parse(location.search).q;
}

export function getPageQueryFromQueryString(location) {
  return parse(location.search).page;
}

export function getTagQueryFromQueryString(location) {
  return parse(location.search).tag;
}

export function buildQueryStringForListMode(location) {
  return "";
}

export function buildQueryStringForTag(location, searchQuery) {
  return "?tag=" + encodeURIComponent(searchQuery);
}

// https://stackoverflow.com/a/38979205
export function isUrlAbsolute(url) {
  if (url.indexOf('//') === 0) {return true;} // URL is protocol-relative (= absolute)
  if (url.indexOf('://') === -1) {return false;} // URL has no protocol (= relative)
  if (url.indexOf('.') === -1) {return false;} // URL does not contain a dot, i.e. no TLD (= relative, possibly REST)
  if (url.indexOf('/') === -1) {return false;} // URL does not contain a single slash (= relative)
  if (url.indexOf(':') > url.indexOf('/')) {return false;} // The first colon comes after the first slash (= relative)
  if (url.indexOf('://') < url.indexOf('.')) {return true;} // Protocol is defined before first dot (= absolute)
  return false; // Anything else must be relative
}

//=====================================
// Ordering, Grouping, and Sorting
//=====================================

export function mapToObject(map) {
  let result = {};

  for (let [key,val] of map.entries()){
    result[key]= val;
  }

  return result;
}

export function orderByDateAscending(list, keyGetter) {
  return list.sort(function(a,b) {
    return (new Date (keyGetter(a))) - (new Date (keyGetter(b)))
  });
}

export function orderByDateDescending(list, keyGetter) {
  return list.sort(function(a,b) {
    return ((new Date (keyGetter(b)) - new Date (keyGetter(a))))
  });
}

// https://stackoverflow.com/a/38327540
export function groupByMonth(list, keyGetter) {
  const map = new Map();

  list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
          map.set(key, [item]);
      } else {
          collection.push(item);
      }
  });

  return map;
}

//=====================================
// Search
//=====================================

export function search(notes, searchQuery) {
  var options = {
      shouldSort: true,
      threshold: 0.6,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: [
        "blocks.text",
        "tags"
    ]
  };

  var fuse = new Fuse(notes, options);
  return fuse.search(searchQuery);
}

//=====================================
// Tags
//=====================================

export function buildTagsArray(notesMap) {
  var tags = {};

  for (const note of Object.values(notesMap)){ 
    if (note.tags && note.tags.length && note["fn-status"] !== "deleted") {
      tags = new Set([...tags, ...new Set(note.tags)])
    }    
  }

  console.debug("buildTagsArray", tags);

  return [...tags].sort();
}

export function getHashTags(text) {
  if (!text) return [];

  const matches = text.match(/(^|\s|>)(#[a-zA-Z0-9_/-]+)/g);
  if (!matches) return [];

  return matches.map(s => s.replace('>', '').trim());
}

//=====================================
// Misc
//=====================================
export function uuid() {
  /*jshint bitwise:false */
  var i, random;
  var uuid = '';

  for (i = 0; i < 32; i++) {
    random = Math.random() * 16 | 0;
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      uuid += '-';
    }
    uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
      .toString(16);
  }

  return uuid;
}

