// https://github.com/visualjerk/quill-magic-url/blob/master/src/index.js

import Quill from 'quill'
import Delta from 'quill-delta'
import normalizeUrl from 'normalize-url'

const defaults = {
  globalRegularExpression: /(https?:\/\/|www\.)[\S]+/g,
  urlRegularExpression: /(https?:\/\/[\S]+)|(www.[\S]+)/,
  hashTagRegularExpression: /(?:^| )(#[a-zA-Z0-9_/-]+)/,
  normalizeRegularExpression: /(https?:\/\/[\S]+)|(www.[\S]+)/,
  normalizeUrlOptions: {
    stripFragment: false,
    stripWWW: false
  }
}

export default class MagicUrl {
  constructor (quill, options) {
    this.quill = quill
    options = options || {}
    this.options = {...defaults, ...options}
    this.registerTypeListener()
    this.registerPasteListener()
  }

  registerPasteListener () {
    this.quill.clipboard.addMatcher(Node.TEXT_NODE, (node, delta) => {
      if (typeof node.data !== 'string') {
        return
      }
      const matches = node.data.match(this.options.globalRegularExpression)
      if (matches && matches.length > 0) {
        const newDelta = new Delta()
        let str = node.data
        matches.forEach(match => {
          const split = str.split(match)
          const beforeLink = split.shift()
          newDelta.insert(beforeLink)
          newDelta.insert(match, {link: match})
          str = split.join(match)
        })
        newDelta.insert(str)
        delta.ops = newDelta.ops
      }
      return delta
    })
  }

  registerTypeListener () {
    this.quill.on('text-change', (delta) => {
      let ops = delta.ops
      // Only return true, if last operation includes whitespace inserts
      // Equivalent to listening for enter, tab or space
      if (!ops || ops.length < 1 || ops.length > 2) {
        return
      }
      let lastOp = ops[ops.length - 1]
      if (!lastOp.insert || typeof lastOp.insert !== 'string' || !lastOp.insert.match(/\s/)) {
        return
      }
      this.checkTextForUrl()
    })
  }

  checkTextForUrl () {
    let sel = this.quill.getSelection()
    if (!sel) {
      return
    }

    let [leaf] = this.quill.getLeaf(sel.index)
    if (!leaf.text) {
      return
    }
  
    let urlMatch = leaf.text.match(this.options.urlRegularExpression);

    // No URL autocomplete within code blocks
    if (urlMatch && leaf.parent.constructor.name === "Block") {
      let stepsBack = leaf.text.length - urlMatch.index;
      let index = sel.index - stepsBack;
      this.textToUrl(index, urlMatch[0]);
    }

    let hashTagMatch = leaf.text.match(this.options.hashTagRegularExpression);
    let hashTag = hashTagMatch && hashTagMatch.length === 2 && hashTagMatch[1];

    // No hashtag autocomplete within code blocks
    if (hashTag && leaf.parent && leaf.parent.domNode.localName === "p") {
      let trimmed = leaf.text.trimStart();
      let stepsBack = trimmed.length;
      let index = sel.index - stepsBack;
      this.textToHashtag(index, hashTag);
    }
  }

  textToUrl (index, url) {
    const ops = new Delta()
      .retain(index)
      .delete(url.length)
      .insert(url, {link: this.normalize(url)})
    this.quill.updateContents(ops)
  }

  textToHashtag (index, hashTag) {
    let url = '/?tag=' + hashTag.replace('#', '%23');
    
    const ops = new Delta()
      .retain(index)
      .delete(hashTag.length)
      .insert(hashTag, {link: this.normalize(url)})

    this.quill.updateContents(ops)
  }

  normalize (url) {
    if (this.options.normalizeRegularExpression.test(url)) {
      return normalizeUrl(url, this.options.normalizeUrlOptions)
    }
    return url
  }
}

Quill.register('modules/magicUrl', MagicUrl);