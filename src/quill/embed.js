import Quill from 'quill';
import remoteStorage from '../Store';

let BlockEmbed = Quill.import('blots/block/embed');

// https://stackoverflow.com/a/45425891

export class ImageBlot extends BlockEmbed {
  static create(value) {
    let node = super.create(value);

    if (typeof value === 'string') {
      node.setAttribute('src', value);
    } else {
      if (value.name) {
        node.setAttribute('name', value.name);
        node.setAttribute('src', '/static/images/loader.gif');

        remoteStorage.images.getObjectUrl(value.name).then(url => {
          node.setAttribute('src', url);
        });
      }
    }

    node.classList.add('mx-auto');
    node.classList.add('d-block');
    node.classList.add('img-fluid');

    return node;
  }

  static value(node) {
    return {
      name: node.getAttribute('name')
    } 
  }
}

ImageBlot.blotName = 'image';
ImageBlot.tagName = 'img';

Quill.register(ImageBlot);