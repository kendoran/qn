import Quill from 'quill';
import Delta from 'quill-delta';

//var Emitter = Quill.import('core/emitter');
var Module = Quill.import('core/module');

class Uploader extends Module {
  constructor(quill, options) {
    super(quill, options);

    quill.root.addEventListener('paste', e => {
      const images = Array.from(e.clipboardData.files || [])
                          .filter(x => options.mimetypes.includes(x.type)); 

      if (!images.length) return;
      e.preventDefault(); // Only preventDefault if this is an image.

      let range = quill.getSelection(true);
      this.upload(range, images);
    });

    quill.root.addEventListener('drop', e => {
      e.preventDefault();
      let native;
      if (document.caretRangeFromPoint) {
        native = document.caretRangeFromPoint(e.clientX, e.clientY);
      } else if (document.caretPositionFromPoint) {
        const position = document.caretPositionFromPoint(e.clientX, e.clientY);
        native = document.createRange();
        native.setStart(position.offsetNode, position.offset);
        native.setEnd(position.offsetNode, position.offset);
      } else {
        return;
      }
      const normalized = quill.selection.normalizeNative(native);
      const range = quill.selection.normalizedToRange(normalized);
      this.upload(range, e.dataTransfer.files);
    });
  }

  upload(range, files) {
    const uploads = [];
    Array.from(files).forEach(file => {
      if (file && this.options.mimetypes.includes(file.type)) {
        uploads.push(file);
      }
    });
    if (uploads.length > 0) {
      this.options.handler.call(this, range, uploads);
    }
  }
}

Uploader.DEFAULTS = {
  mimetypes: ['image/png', 'image/jpeg'],
  handler(range, files) {
    const promises = files.map(file => {
      return new Promise(resolve => {
        const reader = new FileReader();
        reader.onload = e => {
          resolve(e.target.result);
        };
        reader.readAsDataURL(file);
      });
    });
    Promise.all(promises).then(images => {
      const update = images.reduce((delta, image) => {
        return delta.insert({ image });
      }, new Delta().retain(range.index).delete(range.length));
      this.quill.updateContents(update, 'user'); // Emitter.sources.USER
      this.quill.setSelection(
        range.index + images.length,
        'silent', // Emitter.sources.SILENT
      );
    });
  },
};

Quill.register('modules/uploader', Uploader);

export default Uploader;