import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap/dist/css/bootstrap-theme.css';
// import {$,jQuery} from 'jquery';
// // export for others scripts to use
import 'bootstrap';

import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Route } from 'react-router-dom'

import remoteStorage from './Store'

import './typography.css';
import './index.css';
import App from './App';


// https://www.sohamkamani.com/blog/2017/03/31/react-redux-connect-explained/
const Root = () => (
    <Router>
        <Route path="/" render={props => <App {...props} remoteStorage={remoteStorage} />} />
    </Router>
)

ReactDOM.render(
    <Root remoteStorage={remoteStorage} />,
    document.getElementById('root')
);
