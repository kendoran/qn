import RemoteStorage from 'RemoteStorage';

// https://remotestorage.github.io/remotestorage.js/files/baseclient-js.html
// https://media.readthedocs.org/pdf/remotestoragejs/latest/remotestoragejs.pdf

var NoteSchema = {
    "type": "object",
    "properties": {
        "id": {
            "type": "string"
        },
        "title": {
            "type": "string"
        },
        "createdTime": {
            "type": "string",
        },
        "modifiedTime": {
            "type": "string",
        },
        // TODO: Need to make agendaTime optional
        // "agendaTime": {
        //     "type": "string",
        //     "default": null,
        // },
        "contentUrl": {
            "type": "string",
        },
        "tags": {
            "type": "array",
            "default": []
        },
        "blocks": {
            "type": "array",
            "default": []
        },
    },
};

var NoteStore = {   
    name: 'notes',
    builder: function(privateClient, publicClient) {
        privateClient.declareType('note', NoteSchema);

        return {
            exports: {
                store: function (note) {
                    return privateClient.storeObject('note', note.id, note);
                },
                remove: function(note) {
                    return privateClient.remove(note.id);
                },
                get: function(id) {
                    return privateClient.getObject(id);
                },
                getAll: function() {
                    return privateClient.getAll('');
                },
                getListing: function() {
                    return privateClient.getListing('');
                },
                privateClient: privateClient,
            }
        };
    },
};

let ImageStore = { name: 'images', builder: function(privateClient, publicClient) {
    let imagesMap = {}

    return {
      exports: {
       storeFile: function(mimeType, fileName, body) {
           return privateClient.storeFile(mimeType, fileName, body);
       },
       getObjectUrl: function(fileName) {
            if (imagesMap[fileName]) {
                return new Promise(function(resolve, reject) {
                    resolve(imagesMap[fileName]);
                });
            }
            else {
                return privateClient.getFile(fileName).then(file => {
                      let blob = new Blob([file.data], { type: file.contentType });
                      return window.URL.createObjectURL(blob);
                });
            }
       },
       imagesMap,
       privateClient: privateClient,
      }
    }
}};

const remoteStorage = new RemoteStorage({ logging: true, requestTimeout: 90000, modules: [ NoteStore, ImageStore ] });
remoteStorage.access.claim('notes', 'rw');
remoteStorage.caching.enable('/notes/');

remoteStorage.access.claim('images', 'rw');
remoteStorage.caching.enable('/images/');

remoteStorage.images.privateClient.on('change', function(e) {
    console.debug("RemoteStorage " + e.origin + " change event", e);
    
    if (e.newValue && e.newContentType) {
        let blob = new Blob([e.newValue], { type: e.newContentType });
        let src = window.URL.createObjectURL(blob);
        remoteStorage.images.imagesMap[e.relativePath] = src;
    }
});

window.remoteStorage = remoteStorage;

export default remoteStorage;