import moment from 'moment';

import { getHashTags, groupByMonth, isUrlAbsolute, orderByDateDescending, mapToObject } from './Utils';

it('correctly fetches hash tags', () => {
    var content = `
        <div>
            <span>Some HTML here #bee</span>
            http://foo.bar/#anchor
        #bee/matt</closingTag> #bar #tag-two #tag4 <a>#tag5</a></div>
        no#tag #tag_6 
        `;

    var hashTags = getHashTags(content);

    expect(hashTags[0]).toEqual("#bee")
    expect(hashTags[1]).toEqual("#bee/matt")
    expect(hashTags[2]).toEqual("#bar")
    expect(hashTags[3]).toEqual("#tag-two")
    expect(hashTags[4]).toEqual("#tag4")
    expect(hashTags[5]).toEqual("#tag5")
    expect(hashTags[6]).toEqual("#tag_6")
  });
  

it ('correctly orders by date', () => {
    var list = [
        { agendaTime: "2018-09-11T08:43:21.000Z"},
        { agendaTime: "2018-08-10T08:43:21.000Z"},
        { agendaTime: "2018-08-11T08:43:21.000Z"},
    ]

    var sorted = [
        { agendaTime: "2018-09-11T08:43:21.000Z"},
        { agendaTime: "2018-08-11T08:43:21.000Z"},
        { agendaTime: "2018-08-10T08:43:21.000Z"},    
    ]

    var ordered = orderByDateDescending(list, x => x.agendaTime);

    expect(ordered).toEqual(sorted);
});

it('correctly groups by month', () => {
    var list = [
        { agendaTime: "2018-09-11T08:43:21.000Z"},
        { agendaTime: "2018-08-11T08:43:21.000Z"},
    ];

    var expected = {
        "September 2018": [
            {"agendaTime": "2018-09-11T08:43:21.000Z"}
        ],
        "August 2018": [
            {"agendaTime": "2018-08-11T08:43:21.000Z"}
        ]
    };

    var grouped = groupByMonth(list, x => moment(x.agendaTime).format('MMMM YYYY'))
    var groupedAsObject = mapToObject(grouped)

    expect(groupedAsObject).toEqual(expected)
});

it('correctly detects absolute urls', () => {
    expect(isUrlAbsolute('http://stackoverflow.com')).toEqual(true);
    expect(isUrlAbsolute('//stackoverflow.com')).toEqual(true);
    expect(isUrlAbsolute('stackoverflow.com')).toEqual(false);
    expect(isUrlAbsolute('Ftp://example.net') ).toEqual(true);
    expect(isUrlAbsolute('/redirect?target=http://example.org')).toEqual(false);
});

