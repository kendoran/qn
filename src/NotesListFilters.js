import { buildQueryStringForNote, buildQueryStringForCloseNote, MODE, PAGEQUERY, search, orderByDateAscending, orderByDateDescending, groupByMonth, getNoteIdFromQueryString, getPageQueryFromQueryString, getTagQueryFromQueryString } from "./Utils"
import moment from 'moment';

export function getAgendaTitleText(agendaTime) {
    let calendarSettings = {
        lastDay : '[Yesterday]',
        sameDay : '[Today]',
        nextDay : '[Tomorrow]',
        lastWeek : '[last] dddd',
        nextWeek : 'dddd',
        sameElse : 'MMMM Do YYYY'
    };
  
    return moment(agendaTime).calendar(null, calendarSettings);
}

export function filterAgenda(pageQuery, notes) {
    if (pageQuery === PAGEQUERY.TODAY) {
        const today = moment();
        const result = notes.filter(note => note.agendaTime && today.isSame(note.agendaTime, 'day'));
        return result;
    } else if (pageQuery === PAGEQUERY.AGENDA) {
        const result = notes.filter(note => note.agendaTime);
        return result;
    } else {
        return notes;
    }
}

export function filterNotes(props, notes) {
    let pageQuery = getPageQueryFromQueryString(props.location)

    if (props.currentMode !== MODE.SEARCH) {
        if (pageQuery) {
            return filterAgenda(pageQuery, notes);
        }
    }
    else if (props.searchQuery) {
        return search(notes, props.searchQuery);
    }

    return notes;
}

export function filterTag(props, notes) {
    let tag = getTagQueryFromQueryString(props.location);
    if (!tag) return notes;

    let currentNoteId = getNoteIdFromQueryString(props.location);

    return notes.filter(note => note.id === currentNoteId
                                || (note.tags && note.tags.includes(tag)));
}