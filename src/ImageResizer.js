import md5 from "crypto-js/md5";
import { stringify } from "crypto-js/enc-base64";

// https://stackoverflow.com/questions/15341912/how-to-go-from-blob-to-arraybuffer

var dataURLToBlob = function(dataURL) {
    let BASE64_MARKER = ';base64,';
  
    if (dataURL.indexOf(BASE64_MARKER) === -1) {
      let parts = dataURL.split(',');
        let contentType = parts[0].split(':')[1];
        let raw = parts[1];
  
        return new Blob([raw], {type: contentType});
    }
  
    let parts = dataURL.split(BASE64_MARKER);
    let contentType = parts[0].split(':')[1];
    let raw = window.atob(parts[1]);
    let rawLength = raw.length;
  
    let uInt8Array = new Uint8Array(rawLength);
  
    for (let i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }
  
    return new Blob([uInt8Array], {type: contentType});
}

let mimeTypeToExtMap = {
    'image/jpeg': 'jpg',
    'image/png': 'png',
    'image/gif': 'gif',
}
  
export function resizeImage(file) {
    return new Promise(function(resolve, reject) {
      var reader = new FileReader();
      reader.onload = function (readerEvent) {
          var image = new Image();
          image.onload = function (imageEvent) {
              // Resize the image
              var canvas = document.createElement('canvas'),
                  max_size = 800,
                  width = image.width,
                  height = image.height;

              if (width > height) {
                  if (width > max_size) {
                      height *= max_size / width;
                      width = max_size;
                  }
              } else {
                  if (height > max_size) {
                      width *= max_size / height;
                      height = max_size;
                  }
              }

              width = Math.round(width);
              height = Math.round(height);

              canvas.width = width;
              canvas.height = height;
              canvas.getContext('2d').drawImage(image, 0, 0, width, height);
              var dataUrl = canvas.toDataURL(file.type);
              var resizedImageBlob = dataURLToBlob(dataUrl);
    
              var blobFileReader = new FileReader();
              blobFileReader.onload = function(event) {
                  // md5 checksum of original file - it might change with
                  // resizing, so better off using the original.
                  let originalFileMd5 = md5(readerEvent.target.result).toString();
                  let filename = originalFileMd5 + '-' + width + 'x' + height + '.' + mimeTypeToExtMap[file.type];

                  resolve({
                    filename,
                    width,
                    height,
                    originalFileMd5,
                    data: event.target.result
                  });
              };
              blobFileReader.readAsArrayBuffer(resizedImageBlob);
          }
          image.src = readerEvent.target.result;
      }
      reader.readAsDataURL(file);
    });
}