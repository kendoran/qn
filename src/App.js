import React, { Component } from 'react';

import AddNoteButton from './components/AddNoteButton'
import ListModeTopNav from './components/ListModeTopNav'
import MobileSearchModeTopNav from './components/MobileSearchModeTopNav'
import MobileNoteEdit from './components/MobileNoteEdit'
import { NotesList } from './components/NotesList'
import { CreateQuillNote, CreateQuillConflict } from './components/QuillEditor';
import { Sidebar, SidebarBackground, SidebarMargin } from './components/Sidebar';
import { DEVICE, MODE, buildTagsArray, buildQueryStringForListMode, buildQueryStringForNoteId, getCurrentMode, getNoteIdFromQueryString, getHashTags, isEmptyObject, uuid, getPageQueryFromQueryString, getSearchQueryFromQueryString, getTagQueryFromQueryString, getWindowWidth } from './Utils'
import { PAGEQUERY, orderByDateAscending, orderByDateDescending, groupByMonth } from "./Utils"

import {filterNotes, filterTag, getAgendaTitleText} from './NotesListFilters'
import moment from 'moment';

import './App.css';
import './quill/mention'

function UpdateReadyAlert(props) {
  function reload() {
    window.location.reload();
  }

  return (
    <div class="fn-list-item alert alert-info mt-4" role="alert">
      A new version of the app is available <a href="#" className="alert-link" onClick={reload}>click here</a> to update.
    </div>
  );
}

class App extends Component {
  constructor(props) {
    super(props);

    const baseUrl = process.env.REACT_APP_SERVER_BASE_URL;
    const signInUrl = baseUrl + 'login';
    const signOutUrl = baseUrl + 'logout';
    const defaultContentUrl = baseUrl + 'note/content';

    this.state = {
      baseUrl: baseUrl,

      isUserInfoLoaded: false,
      userInfoUrl: baseUrl + 'user_info',
      currentUser: {},
      isSignedIn: false,

      tags: {},

      notesMap: {},
      notesListGroupedItems: new Map(),

      updatedNotes: {},

      saveStatusMessage: 'Saved',
      signInUrl: signInUrl,
      signOutUrl: signOutUrl,

      defaultContentUrl: defaultContentUrl,

      remoteStorage: null,
      isRemoteStorageSignedIn: false,
      remoteStorageLastSynced: null,
      
      isSidebarActive: false,
      isOverlaySidebarActive: false,

      addNoteUrl: '',
      nextNoteId: uuid(),

      listModeUrl: '',

      searchModeBaseUrl: '?q=',

      currentMode: MODE.LIST,

      atValues: [
        { id: 3, value: 'Matt Button' },
        { id: 1, value: 'Fredrik Sundqvist' },
        { id: 2, value: 'Patrik Sjölin' }
      ],

      hashValues: [],
    }
  }

  //============================
  // Initial Page Load
  //============================

  componentDidMount = () => {
    console.debug("app.js componentDidMount");

    if (!this.props.location.search) {
      this.props.history.push("/?page=today");
    }

    this.setState({
      remoteStorage: this.setupRemoteStorage(this.props.remoteStorage),
      currentNoteId: getNoteIdFromQueryString(this.props.location),
      addNoteUrl: buildQueryStringForNoteId(this.props.location, this.state.nextNoteId, true),
      pageQuery: getPageQueryFromQueryString(this.props.location),
      searchQuery: getSearchQueryFromQueryString(this.props.location),
      isSidebarActive: getWindowWidth() >= DEVICE.LARGE,
    });

    this.refreshCurrentUser();

    window.setInterval(this.processUpdatedNotesQueue, 1000);
    window.setInterval(this.processUpdatedNotesMap, 1000);

    window.applicationCache.addEventListener('updateready', this.onUpdateReady);
    if(window.applicationCache.status === window.applicationCache.UPDATEREADY) {
      this.onUpdateReady();
    }
  }

  checkForUpdates() {
    window.applicationCache.update();
  }

  onUpdateReady = () => {
    console.debug("onUpdateReady");
    this.setState({ isUpdateReady: true });
  }

  componentDidUpdate(prevProps, prevState) {
    let mode = getCurrentMode(this.props.location);
    if (this.state.currentMode !== mode) {
      this.setState({ currentMode: mode });
    }

    let newNoteId = getNoteIdFromQueryString(this.props.location);
    if (prevState.currentNoteId !== newNoteId) {
      this.setState({ currentNoteId: newNoteId });
    }

    let searchQuery = getSearchQueryFromQueryString(this.props.location);
    if (prevState.searchQuery !== searchQuery) {
      this.setState({
        listModeUrl: buildQueryStringForListMode(this.props.location),
        searchQuery,
      });
    }

    let pageQuery = getPageQueryFromQueryString(this.props.location);
    if (prevState.pageQuery !== pageQuery) {
      this.setState({
        pageQuery,
        addNoteUrl: buildQueryStringForNoteId(this.props.location, this.state.nextNoteId, true),
      });

      this.refreshNotesListGroupedItems();
    }

    let tagQuery = getTagQueryFromQueryString(this.props.location);
    if (prevState.tagQuery !== tagQuery) {
      this.setState({
        tagQuery,
      });

      this.refreshNotesListGroupedItems();
    }

    if (prevState.notesMap !== this.state.notesMap) {
      this.setState({
        notesMapHasUpdated: true,
      });
    }

    if (prevState.searchQuery !== this.state.searchQuery) {
      console.debug("Search query updated:");
      console.debug(this.state.searchQuery);
    }
  }

  setupRemoteStorage = (remoteStorage) => {
    // https://github.com/skddc/webmarks/blob/master/app/services/storage.js
    // https://github.com/litewrite/litewrite/blob/bbbd44442794775cc97a3fe032602098fdcc6ea9/src/litewrite.js#L38

    // https://remotestoragejs.readthedocs.io/en/latest/js-api/remotestorage.html#events
    
    const t = this;

    remoteStorage.on('ready', () => {
      console.debug('RemoteStorage ready');
    });

    remoteStorage.on('network-offline', () => {
      console.debug(`RemoteStorage network-offline`);
    });
    
    remoteStorage.on('network-online', () => {
      console.debug(`RemoteStorage network-online`);
    });

    remoteStorage.on('connected', () => {
      const userAddress = remoteStorage.remote.userAddress;
      console.debug(`RemoteStorage ${userAddress} connected`); 
      this.setState({ isRemoteStorageSignedIn: true });
    });

    remoteStorage.on('not-connected', () => {
      console.debug('RemoteStorage not-connected');
      this.setState({ isRemoteStorageSignedIn: false });
    });

    remoteStorage.on('error', err => {
      console.error("RemoteStorage error", err);
    });

    remoteStorage.on('sync-req-done', () => {
      console.debug('RemoteStorage sync-req-done');
      this.setState({ remoteStorageSyncing: true });
    });

    remoteStorage.on('sync-done', () => {
      console.debug('RemoteStorage sync-done');

      this.setState({ 
        remoteStorageSyncing: false,
        remoteStorageLastSynced: new Date(),
      });

      t.processUpdatedNotesQueue();
    });

    remoteStorage.notes.privateClient.on('change', function(e) {
      // If there was a server update, and the client didn't receive it, the etags will not match
      // but the modified timestamps will. We can safely assume the note content is the same in this
      // case, and treat it as a regular update, so we get the new etag in IndexDB.
      let conflictTimestampsMatch = e.origin === 'conflict' && e.oldValue.modifiedTime === e.newValue.modifiedTime;

      // https://github.com/remotestorage/remotestorage.js/blob/170f0620da2322a36d3ddc0553d2e809279a03d6/doc/js-api/base-client.rst#conflict
      if (e.origin === 'conflict' && !conflictTimestampsMatch) {
        console.error("Conflict for note: " + e.newValue.id, e)
        console.debug("Can't handle conflicts yet. Creating new note.");

        let block = e.oldValue.blocks[0];
        
        if (block.type === 'quill' || block.type === 'quill-conflict') {
          let oldContent = e.oldValue.blocks[0].content;
          let newContent = e.newValue.blocks[0].content;

          let quillConflict = CreateQuillConflict(t.state.defaultContentUrl, oldContent, newContent)
          remoteStorage.notes.store(quillConflict);

          t.enqueueUpdatedNote(quillConflict);
        }
      }
      else {
        console.debug("RemoteStorage " + e.origin + " change event", e);

        if (e.newValue) {
          t.enqueueUpdatedNote(e.newValue);
        }
        else if (e.oldValue) {
          t.deleteFromNotesMap(e.oldValue.id);
        }
        else {
          console.error("Unrecognised change event");
        }
      }
    });

    return remoteStorage;
  }

  enqueueUpdatedNote = (updatedNote) => {
    console.debug('FN enqueueUpdatedNote');

    if (!updatedNote || !updatedNote.id) {
      console.error("FN enqueueUpdatedNote invalid note: ", updatedNote);
      return;
    }

    if (updatedNote.blocks && updatedNote.blocks.length > 0) {
      let block = updatedNote.blocks[0];

      if (!block.type) {
        updatedNote.tags = getHashTags(block.content)
      }
      else if (block.type === 'quill') {
        updatedNote.tags = getHashTags(block.text);
      }
    }
    
    this.setState((prevState, props) => {
      return {
        updatedNotes: {
          ...prevState.updatedNotes,
          [updatedNote.id]: updatedNote
        }
      }
    });
  }

  deleteNote = (note) => {
    const t = this;

    return function () {
      console.debug("DELETING ", note);
      t.deleteNoteById(note.id);
    }
  }

  deleteNoteById = (noteId) => {
    let note = this.state.notesMap[noteId];
    this.deleteFromNotesMap(noteId);

    this.removeNote(note);
  }

  deleteFromNotesMap = (noteId) => {
    let notesMap = {...this.state.notesMap};
    delete notesMap[noteId];

    this.setState({
      notesMap: notesMap,
    });
  }

  deleteCurrentNote = e => {
    e.stopPropagation();

    this.state.currentNoteId && this.deleteNoteById(this.state.currentNoteId);

    this.setState({
      currentNoteId: null,
    });
  }

  processUpdatedNotesQueue = () => {
    if (isEmptyObject(this.state.updatedNotes)) return;

    console.debug("FN processUpdatedNotesQueue", this.state.updatedNotes);

    const notesMap = Object.assign({}, this.state.notesMap, this.state.updatedNotes);
    const tags = buildTagsArray(notesMap);
    const hashValues = tags.map((x, i) => { return { index: i, value: x.substring(1) } });

    this.setState({
      notesMap: notesMap,
      notesMapHasUpdated: true,
      tags,
      hashValues,
      updatedNotes: {},
    });
  }

  processUpdatedNotesMap = () => {
    if (!this.state.notesMapHasUpdated) return;

    this.refreshNotesListGroupedItems();
  }

  refreshNotesListGroupedItems = () => {
    console.debug("refreshNotesListGroupedItems");

    let notesMap = this.state.notesMap;

    let notesListItems = Object.values(notesMap)
                                .filter(x => x['fn-status'] !== "deleted");

    let filtered = filterNotes(this.props, notesListItems);
    let filteredOnTag = filterTag(this.props, filtered);

    let pageQuery = getPageQueryFromQueryString(this.props.location)

    let ordered = pageQuery === PAGEQUERY.AGENDA 
                    ? orderByDateDescending(filteredOnTag, x => x.agendaTime)
                    : orderByDateDescending(filteredOnTag, x => x.createdTime);

    let grouped = pageQuery === PAGEQUERY.AGENDA 
                    ? groupByMonth(ordered, x => getAgendaTitleText(x.agendaTime))
                    : groupByMonth(ordered, x => moment(x.createdTime).format('MMMM YYYY'));

    this.setState({
      notesListGroupedItems: grouped,
      notesMapHasUpdated: false
    });
  }

  refreshCurrentUser = () => {
    const t = this;

    return fetch(this.state.userInfoUrl, {
      method: 'get',
      credentials: 'include',
      headers: {
        'content-type': 'application/json',
      },
    }).then(function(response) {
      return response.json();
    }).then(function(getCurrentUserResponse) {
      t.setState({
        currentUser: getCurrentUserResponse,
        isUserInfoLoaded: true,
      });

      if (getCurrentUserResponse.isSignedIn && !t.state.isRemoteStorageSignedIn) {
        t.state.remoteStorage.connect("user@" + window.location.host);
      }

      if (!getCurrentUserResponse.isSignedIn && t.state.isRemoteStorageSignedIn) {
        t.state.remoteStorage.disconnect();
      }

      return getCurrentUserResponse;
    });
  }

  //============================
  // Initialising new notes
  //============================

  addNote = () => {
    const newNote = CreateQuillNote(this.state.defaultContentUrl, this.state.nextNoteId);

    let pageQuery = getPageQueryFromQueryString(this.props.location);
    if (pageQuery === PAGEQUERY.AGENDA || pageQuery === PAGEQUERY.TODAY) {
      newNote.agendaTime = moment().format();
    }

    const notesMap = {
            ...this.state.notesMap,
            [newNote.id]: newNote
          };

    const nextNoteId = uuid();

    this.setState({
      notesMap: notesMap,
      tags: buildTagsArray(notesMap),
      nextNoteId: nextNoteId,
      addNoteUrl: buildQueryStringForNoteId(this.props.location, nextNoteId, true),
    });
    
    this.editNote(newNote);

    this.state.remoteStorage.notes
      .store(newNote)
      .then(() => {
        console.debug('stored note successfully');
      })
      .catch((err) => {
        console.error('note validation error:', err);
      });
  }

  addEmptyNote = () => {
    this.addNote();
  }

  //============================
  // Loading notes
  //============================

  editNote = note => {
    this.setState({
      currentNoteId: note.id,
    });
  }

  setCurrentEditor = editor => {
    this.setState({ currentEditor: editor });
  }

  focusCurrentEditor = () => {
      console.debug("focusCurrentEditor", this.state.currentEditor);
      this.state.currentEditor && this.state.currentEditor.focus();
  }

  //============================
  // Saving Notes
  //============================ 

  blockUpdatedNoteBuilder = (originalNote, getBlock) => {
    return function() {
      let block = getBlock();

      return { 
        ...originalNote,
        modifiedTime: new Date().toISOString(),
        tags: block.tags,
        blocks: [
          block
        ]
      };
    }
  }

  onUpdateNoteBlock = (originalNote, getBlock) => {
    console.debug("onUpdateNoteBlock");

    let block = getBlock();

    let note = { 
      ...originalNote,
      modifiedTime: new Date().toISOString(),
      tags: block.tags,
      blocks: [
        block
      ]
    };

    this.storeNote(note);
  }

  onUpdateAgendaTime = (originalNote) => {
    const t = this;

    return function (value) {
      let note = { 
        ...t.state.notesMap[originalNote.id],
        agendaTime: value && value.toISOString(),
      }

      // For some reason, setting agendaTime to null
      // was causing errors relating to "maximum update depth"
      // on setState. deleting the property avoid the error.
      // No idea what the real difference is, but seems to work.
      if (!note.agendaTime) {
        delete note.agendaTime;
      }

      console.debug('onUpdateAgendaTime', note);
      t.storeNote(note);
    }
  }

  removeNote = (note) => {
    this.state.remoteStorage.notes
        .remove(note)
        .then(() => {
          console.debug('removeNote successful');
        })
        .catch((err) => {
          console.error('removeNote error', err);
        });
  }

  storeNote = (note) => {
    let notesMap = {
      ...this.state.notesMap,
      [note.id]: note,
    }

    const tags = buildTagsArray(notesMap);
    const hashValues = tags.map((x, i) => { return { index: i, value: x.substring(1) } })

    this.setState({
      notesMap,
      hashValues,
      tags,
    });

    this.state.remoteStorage.notes
      .store(note)
      .then(() => {
        console.debug('storeNote successful');
      })
      .catch((err) => {
        console.error('storeNote validation error:', err);
      });
  }

  //============================
  // Search
  //============================ 

  clearSearch = () => {
    this.setState({
      searchQuery: '',
      tagQuery: '',
    });
  }

  handleSearchQueryChange = (e) => {
    this.setState({
      searchQuery: e.target.value,
    });
  }

  onTagClick = (tag) => {
    this.setState({
      tagQuery: tag,
    });

    this.onSidebarButtonClick();
  }

  onSidebarButtonClick = () => {
    if (getWindowWidth() < DEVICE.LARGE) {
      this.hideSidebar();
    }
  }


  //============================
  // Rendering
  //============================ 

  hideSidebar = () => {
    if (getWindowWidth() >= DEVICE.EXTRA_LARGE) {
      this.setState({
        isSidebarActive: false,
      });
    } else{
      this.setState({
        isOverlaySidebarActive: false,
      });
    }
  }

  toggleSidebar = () => {
    let isSidebarActive = getWindowWidth() >= DEVICE.EXTRA_LARGE ? !this.state.isSidebarActive : this.state.isSidebarActive;
    let isOverlaySidebarActive = getWindowWidth() < DEVICE.EXTRA_LARGE ? !this.state.isOverlaySidebarActive : this.state.isOverlaySidebarActive;

    this.setState({
      isSidebarActive,
      isOverlaySidebarActive,
    });
  }

  getCurrentNote = () => {
    return this.state.currentNoteId && this.state.notesMap[this.state.currentNoteId];
  }

  render() {
    const noNoteSelected = (
      <div>
        
      </div>
    );

    const addNoteButtonIsVisible = this.state.currentMode === MODE.LIST || this.state.currentMode === MODE.SEARCH || this.state.currentMode === MODE.EDIT_INLINE;

    return (
      <div className="d-flex flex-column h-100">
        <div>
          { this.state.currentMode === MODE.LIST || this.state.currentMode === MODE.EDIT_INLINE ?
            <ListModeTopNav
              addNoteUrl={this.state.addNoteUrl}
              onAddNoteClick={this.addEmptyNote}
              onBarsClick={this.toggleSidebar}
              searchModeUrl={this.state.searchModeBaseUrl}
              onSearchClick={this.clearSearch}
              location={this.props.location}
            /> : "" 
          }
          { this.state.currentMode === MODE.SEARCH ?
            <MobileSearchModeTopNav
              listModeUrl={this.state.listModeUrl}
              searchQuery={this.state.searchQuery}
              onArrowLeftClick={() => {}}
              onSearchQueryChange={this.handleSearchQueryChange}
            />: ""
          }
        </div>

        <div className="d-flex h-100">
          <SidebarBackground
            active={this.state.isOverlaySidebarActive}
            hideSidebar={this.hideSidebar}
          />

          <Sidebar
            active={this.state.isSidebarActive}
            isOverlaySidebarActive={this.state.isOverlaySidebarActive}
            tags={this.state.tags}
            isUserInfoLoaded={this.state.isUserInfoLoaded}
            currentUser={this.state.currentUser}
            signInUrl={this.state.signInUrl}
            signOutUrl={this.state.signOutUrl}
            hideSidebar={this.hideSidebar}
            onTagClick={this.onTagClick}
            onSidebarButtonClick={this.onSidebarButtonClick}
            location={this.props.location} />

          { 
            addNoteButtonIsVisible ?
            <AddNoteButton
                onAddNoteClick={this.addEmptyNote}
                addNoteUrl={this.state.addNoteUrl}
              /> : ""
          }

          <div id="fn-scroll-container" className="fn-app-content">
            <div className={"fn-list-mode " + (this.state.currentMode === MODE.LIST || this.state.currentMode === MODE.SEARCH || this.state.currentMode === MODE.EDIT_INLINE ? "d-block" : "d-none")}>
              
              { this.state.isUpdateReady === true ? <UpdateReadyAlert /> : "" }

              <NotesList
                currentMode={this.state.currentMode}
                currentNoteId={this.state.currentNoteId}
                pageQuery={this.state.pageQuery}
                searchQuery={this.state.searchQuery}
                tagQuery={this.state.tagQuery}
                history={this.props.history}
                location={this.props.location}
                notesListGroupedItems={this.state.notesListGroupedItems}
                onEdit={this.onUpdateNoteBlock}
                onNotesListItemClick={this.editNote}
                atValues={this.state.atValues}
                hashValues={this.state.hashValues}
                onUpdateAgendaTime={this.onUpdateAgendaTime}
                onDeleteClick={this.deleteNote}
              />
            </div>
            {this.state.currentMode === MODE.EDIT_MOBILE 
              ? <div className="fn-mobile-edit-mode col d-block" onClick={this.focusCurrentEditor}>
                  {this.state.currentNoteId ? 
                    <MobileNoteEdit
                      currentNoteId={this.state.currentNoteId}
                      notesMap={this.state.notesMap}
                      onEdit={this.onUpdateNoteBlock}
                      setCurrentEditor={this.setCurrentEditor}
                      focusCurrentEditor={this.focusCurrentEditor}
                      atValues={this.state.atValues}
                      hashValues={this.state.hashValues}
                      history={this.props.history}
                      location={this.props.location}
                      onTrashClick={this.deleteCurrentNote}
                      onUpdateAgendaTime={this.onUpdateAgendaTime}
                      />
                    : noNoteSelected} 
                </div>
              : ""
            }
          </div>
        </div>
      </div>
    );
  }
}

export default App;
