import React from 'react';
import { Link } from 'react-router-dom'

import CKEditor from './CKEditor'
import { QuillEditor, QuillEditorToolbar } from './QuillEditor'

import DatePicker from 'react-datepicker'
import moment from 'moment';

import './MobileNoteEdit.css';

export function MobileAgendaTimePicker(props) {
    let onClearAgendaTime = e => {
        props.onUpdateAgendaTime(props.note)(null);
    }

    return (
        <div>
            <div className="modal fade" id={'mobileAgendaTimePicker-' + props.note.id} tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalCenterTitle">Set Agenda Date</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body mx-auto">
                        <DatePicker
                            inline
                            fixedHeight
                            selected={props.note.agendaTime && moment(props.note.agendaTime)}
                            onSelect={props.onUpdateAgendaTime(props.note)}
                        />
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={onClearAgendaTime}>Clear</button>
                        <button type="button" className="btn btn-primary" data-dismiss="modal">Done</button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export function MobileEditModeTopNav(props) {
    let getText = () => {
        if (!props.note.agendaTime) return '';

        let calendarSettings = {
            lastDay : '[Yesterday]',
            sameDay : '[Today]',
            nextDay : '[Tomorrow]',
            lastWeek : '[last] dddd',
            nextWeek : 'dddd',
            sameElse : 'MMMM Do YYYY'
        };

        return moment(props.note.agendaTime).calendar(null, calendarSettings);
    }

    let stopPropagation = e => {
        e.stopPropagation();
    }

    let goBack = e => {
        e.stopPropagation();
        props.history.goBack();
    }

    let onDeleteClick = e => {
        e.stopPropagation();
        props.history.goBack();
        props.onTrashClick(e);
    }

    return (
        <div className='row fn-top-nav-wrapper' onClick={stopPropagation}>
            <nav className="fn-top-nav fixed-top navbar d-block d-xl-none">
                <div className="btn-toolbar justify-content-between">
                    <div className="btn-group">
                        <button type="button" onClick={goBack} className="fn-search-show btn btn-link"><i className="fas fa-arrow-left"></i></button>
                    </div>
                    <div className="navbar-brand mb-0 h1">Edit</div>
                    <div className="btn-group" role="group">
                        <button type="button" className="btn btn-link" data-toggle="modal" data-target={'#mobileAgendaTimePicker-' + props.note.id}>
                            {getText()} &nbsp;<i className="far fa-calendar-alt"></i>
                        </button>

                        <button className="btn btn-link" type="button" id="dropdownMenuEllipsisButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-ellipsis-v"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuEllipsisButton">
                            <a className="dropdown-item" href="#" onClick={onDeleteClick}>Delete</a>
                        </div>

                        {/* <button className="btn btn-link my-2 my-sm-0" type="submit"><i className="fas fa-sync"></i></button>
                        <button type="button" className="btn btn-link"><i className="fas fa-info"></i></button>
                        <button type="button" className="btn btn-link"><i className="fas fa-question-circle"></i></button> */}
                    </div>
                </div>
            </nav>

            <MobileAgendaTimePicker
                note={props.note}
                agendaTime={props.note.agendaTime}
                onUpdateAgendaTime={props.onUpdateAgendaTime}  />
        </div>
    );
}

export default function MobileNoteEdit(props) {
    function filterForCurrentNote(note) {
        return note.id === props.currentNoteId;
    }

    function stopPropagation(e) {
        e.stopPropagation();
    }

    return (
        <div>
            {
            Object.values(props.notesMap).filter(filterForCurrentNote).map((note, index) =>
            <div key={note.id}>
                <MobileEditModeTopNav 
                    onArrowLeftClick={props.onArrowLeftClick}
                    onTrashClick={props.onTrashClick}
                    note={note}
                    history={props.history}
                    onUpdateAgendaTime={props.onUpdateAgendaTime} />

                {
                    note.blocks && !note.blocks[0].type
                        ? ""
                        : <nav className="fn-quill-toolbar-bottom navbar fixed-bottom navbar-light" onClick={stopPropagation}>
                        <QuillEditorToolbar id={"fn-qt-standalone-" + props.currentNoteId} />
                        </nav>
                }

                <div className="fn-mobile-note-edit">
                    {
                        note.blocks && !note.blocks[0].type
                            ? <CKEditor
                                note={note}
                                content={note.blocks[0].content}
                                onEdit={props.onEdit}
                                /> 
                            : <QuillEditor
                                id={note.id}
                                note={note}
                                onEdit={props.onEdit}
                                toolbarId={"fn-qt-standalone-" + props.currentNoteId}
                                setCurrentEditor={props.setCurrentEditor}
                                atValues={props.atValues}
                                hashValues={props.hashValues} 
                                />
                    }
                </div>
            </div>
            )
            }
        </div>
    )
}