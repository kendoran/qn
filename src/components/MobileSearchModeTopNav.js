import React from 'react';
import { Link } from 'react-router-dom'
import './MobileNoteEdit.css';

export default function MobileSearchModeTopNav(props) {
    return (
        <div className='row fn-top-nav-wrapper'>
            <nav className="fn-top-nav fixed-top navbar d-block">
                <div className="btn-toolbar justify-content-between">
                    <div className="btn-group">
                        <Link to={props.listModeUrl}>
                            <button type="button" onClick={props.onArrowLeftClick} className="fn-search-show btn btn-link"><i className="fas fa-arrow-left"></i></button>
                        </Link>
                    </div>
                    <div className="input-group flex-fill">
                        {/* <div className="input-group-prepend">
                            <div className="input-group-text"><i className="fas fa-search"></i></div>
                        </div> */}

                        <div className="flex-fill">
                            <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"
                                    value={props.searchQuery} onChange={props.onSearchQueryChange} />
                        </div>
                    </div>        
                </div>
            </nav>
        </div>
    );
}