import React, { Component } from 'react';
import debounce from 'lodash/debounce';
import { withRouter } from 'react-router'
import PropTypes from 'prop-types';

import Quill from 'quill'
import Delta from 'quill-delta';
import {ImageBlot} from '../quill/embed'

import List, { ListItem } from '../quill/list';
import '../quill/magic_url'
import '../quill/mention'
import '../quill/uploader'

import { resizeImage } from '../ImageResizer'
import { getHashTags, isNewNote, isUrlAbsolute, uuid } from '../Utils'

import './quill.snow.css';
import './QuillEditor.css';

Quill.register({
  'formats/list': List,
  'formats/list/item': ListItem,
});

let Parchment = Quill.import('parchment');

// https://quilljs.com/docs/modules/toolbar/
export function QuillEditorToolbar(props) {
  return (
    <div id={props.id} className={'editor-toolbar'}>
      <button className="ql-header" value="1"></button>
      <button className="ql-header" value="2"></button>

      <button className="ql-bold"></button>

      <button className="ql-list" value="bullet"></button>
      <button className="ql-list" value="ordered"></button>
      <button className="ql-list" value="check"></button>

      <button className="ql-indent" value="-1"></button>
      <button className="ql-indent" value="+1"></button>

      <button className="ql-blockquote"></button>
      <button className="ql-code-block"></button>

      {/* <button className="ql-link"></button> */}
      <button className="ql-image"></button>

      <button className="ql-clean"></button>

      {/* <button className="ql-script" value="sub"></button>
      <button className="ql-script" value="super"></button> */}
    </div>
  );
}

export function CreateQuillNote(defaultContentUrl, nextNoteId) {
  const today = new Date();

  return {
    id: nextNoteId,
    isNew: true,     
    contentUrl: defaultContentUrl,
    createdTime: today.toISOString(),
    modifiedTime: today.toISOString(),
    tags: [],
    blocks: [
      { 
        content: null,
        type: 'quill'
      }
    ],
  };
}

function imageHandler(t) {
  return async function (range, files) {
    const file = files[0];

    let tt = this;

    resizeImage(file).then(function(result) {
      let resizeResult = result;

      window.remoteStorage.images.storeFile(file.type, result.filename, result.data).then(() => {
        const image = {
          name: resizeResult.filename,
        }
    
        // Remove placeholder image
        tt.quill.deleteText(range.index, 1);
    
        // Insert uploaded image
        let update = new Delta().retain(range.index).delete(range.length).insert({image}).insert('\n');
        tt.quill.updateContents(update, 'user');
        tt.quill.setSelection(range.index + 1); // Ensure cursor is placed after the image
      });
    });
  }
}

export function CreateQuillConflict(defaultContentUrl, oldContent, newContent) {
  const today = new Date();

  return {
    id: uuid(),
    isNew: true,     
    contentUrl: defaultContentUrl,
    createdTime: today.toISOString(),
    modifiedTime: today.toISOString(),
    tags: [],
    blocks: [
      { 
        content: null,
        type: 'quill-conflict'
      }
    ],
  };
}


function altBoundingClientRect() {
  let base = document.body.getBoundingClientRect();
  base.height -= 68; // rough height of the bottom toolbar
  return base;
}

// Overrides getBoundingClientRect in:
// https://github.com/quilljs/quill/blob/41a60fbf7cc9d23f4d87c4a88c42bb56157e3432/core/selection.js#L295
function overrideBindingClientRect(scrollingContainerSelector) {
  document.querySelector(scrollingContainerSelector).getBoundingClientRect = altBoundingClientRect;
}

export class QuillEditorBase extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      instanceId: "fn-quill-" + uuid(),
    }
  }

  initContent = (editor) => {
    if (!this.props.note) return null;
    if (!this.props.note.blocks) return null;

    let block = this.props.note.blocks[0];

    if (block.type === 'quill') {
      block.content && editor.setContents(block.content);
    }
    else if (block.type === 'quill-conflict') {
      editor.pasteHTML("<b>A conflict was found. Resolving conflicts isn't supported yet. This #fn/conflict note was created to avoid losing changes.</b>");
    }
    else {
      console.error("Unrecognised block", block, this.props.note);
    }
  }

  getBlock = () => {
    let text = this.state.editor.getText();

    return {
      type: "quill",
      tags: getHashTags(text),
      text: text,
      contentType: 'application/json',
      content: this.state.editor.getContents()
    }
  }

  buildMentionConfig = () => {
    let t = this;

    return {
      allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
      mentionDenotationChars: ["#"], // "@", 
      source: function (searchTerm, renderList, mentionChar) {
        let values;

        if (mentionChar === "@") {
          values = t.props.atValues || [];
        } else {
          values = t.props.hashValues || [];
        }

        values = t.props.hashValues || [];
        
        if (searchTerm.length === 0) {
          renderList(values, searchTerm);
        } else {
          const matches = [];
          for (var i = 0; i < values.length; i++){
            if (~values[i].value.toLowerCase().indexOf(searchTerm.toLowerCase())) matches.push(values[i]);
          }
            
          renderList(matches, searchTerm);
        }
      }
    }
  }

  // https://github.com/ReactTraining/react-router/blob/master/packages/react-router-dom/modules/Link.js#L35
  onLinkClick = (event) => {
    const { history } = this.props

    let to = event.srcElement.getAttribute('href');
    if (to && !isUrlAbsolute(to)) {
      event.preventDefault();
      history.push(to);
    }
  }

  onImageUploadButtonClick = () => {
    let quill = this.state.editor;

    let fileInput = document.createElement('input');
    fileInput.setAttribute('type', 'file');
    fileInput.setAttribute(
      'accept',
      quill.options.modules.uploader.mimetypes.join(', '),
    );
    fileInput.classList.add('ql-image');
    fileInput.addEventListener('change', () => {
      const range = quill.getSelection(true);
      quill.getModule('uploader').upload(range, fileInput.files);
      fileInput.value = '';
    });

    quill.container.appendChild(fileInput);
    fileInput.click();
    quill.container.removeChild(fileInput);
  }

  componentDidMount = () => {
    let scrollingContainer = this.props.scrollingContainer || '#fn-scroll-container';

    let options = {
        //debug: 'info',
        modules: {
          toolbar: {
            container: this.props.toolbarId ? '#' + this.props.toolbarId : null,
            handlers: {
              image: this.onImageUploadButtonClick
            }
          },
          mention: this.buildMentionConfig(),
          magicUrl: true,
          syntax: true,
          uploader: {
            handler: imageHandler(this)
          },
        },
        placeholder: this.props.readOnly ? '' : 'Write something here',
        theme: 'snow',
        scrollingContainer,
      };

    let editor = new Quill('#' + this.state.instanceId, options);
    this.setState({editor})
    this.initContent(editor);
    this.bindQuillClickEvent(editor);

    overrideBindingClientRect(scrollingContainer);

    const t = this;
    const previewNodes = this.refs.quillElement.querySelectorAll('a.ql-preview');
    if (previewNodes) {
      Array.prototype.forEach.call(previewNodes, function(el, i) {
        el.addEventListener('click', t.onLinkClick, false);
      });
    }
    
    if (this.props.autoFocus || isNewNote(this.props.location)) {
      editor.focus();
    }

    this.props.setCurrentEditor && this.props.setCurrentEditor(editor);

    let callOnEdit = debounce(function() {
      t.props.onEdit && t.props.onEdit(t.props.note, t.getBlock);
      t.setState({ isDirty: false });
    }, 5000);

    this.setState({ callOnEdit });

    editor.on('selection-change', function(range, oldRange, source) {
      window.quill = editor;
    });

    // TODO: Implement more like: https://github.com/reedsy/quill-cursors/blob/master/example/main.js#L20
    // https://stackoverflow.com/questions/44372102/how-to-merge-two-contents-version-with-quill-js
    // https://github.com/share/sharedb
    editor.on('text-change', function(delta, oldDelta, source) {
      if (source === 'user') {
        callOnEdit();
        t.setState({ isDirty: true });
      }
    });
  }

  bindQuillClickEvent(quill) {
    quill.root.addEventListener('click', e => {
      let image = Parchment.find(e.target);
    
        // Support Clickable Images
        if (image instanceof ImageBlot) {
            quill.setSelection(image.offset(quill.scroll), 1, 'user');
        }
    });
  }

  componentWillUnmount = () => {
    console.debug("QuillEditor componentWillUnmount");
    if (this.state.isDirty) {
      console.debug("Saving note before unmount", this.props.note);

      let block = this.getBlock();
      this.props.onEdit && this.props.onEdit(this.props.note, () => { return block });
    }

    this.state.editor.off('text-change');
    this.state.callOnEdit.cancel();
    this.setState({ editor: null });
  }

  shouldComponentUpdate(nextProps, nextState) {
    // Once the editable component has been loaded, I don't want it to receive updates
    // Any updates in the editable area should come from the user. 
    // The QuillEditorReadOnly component receives updates.
    return false;
  }

  render() {
      return (
        <div id={this.state.instanceId} ref="quillElement"></div>
      )
    }
}

const QuillEditor = withRouter(QuillEditorBase);
// https://reacttraining.com/react-router/web/api/withRouter
export { QuillEditor }