import React from 'react';
import { Link } from 'react-router-dom'
import { buildQueryStringForTag } from '../Utils'
import './Sidebar.css'

export function TagsList(props) {
    return (
        <div className="fn-tags-list list-group">
            {
                Object.values(props.tags).map((tag) => 
                    <Link key={tag} to={buildQueryStringForTag(props.location, tag)}
                          onClick={() => props.onTagClick(tag)} className="list-group-item list-group-item-action">
                        {tag}
                    </Link>
                )
            }
        </div>
    );
}

export function SignInButton(props) {
    return (
        <div className="fn-current-user mt-3">
            <div>
                <a href={props.signInUrl} className="btn btn-secondary" role="button">
                    <i className="fas fa-sign-in-alt"></i> Sign In with Google
                </a>
            </div>

            <hr />
        </div>
    )
}

export function CurrentUser(props) {
    return (
        <div className="fn-current-user">
            <span className="fn-current-user-avatar">
                <img src={props.currentUser.picture} alt="avatar" />
            </span>
            <span className="fn-current-user-name align-middle">
                <b>{props.currentUser.name}</b>
                <p>{props.currentUser.email}</p>
            </span>

            <div>
                <a href={props.signOutUrl} className="btn btn-secondary" role="button">
                    <i className="fas fa-sign-out-alt"></i> Sign Out
                </a>
            </div>

            <hr />
        </div>
    );
}

export function SignInSignOutButton(props) {
    return (
        props.currentUser.isSignedIn ?
            <CurrentUser currentUser={props.currentUser} signOutUrl={props.signOutUrl} />
            : <SignInButton signInUrl={props.signInUrl} /> 
    );
}

export function SidebarBackground(props) {
    return (
        <div id='fn-sidebar-background' className={props.active ? '': 'd-none'} onClick={props.hideSidebar}></div>
    );
}

export function SidebarMargin(props) {
    return (
        <div id='fn-sidebar-margin' className={props.active ? 'active': 'd-none'}></div>
    );
}

export function Sidebar(props) {
    let active = props.active ? 'active h-100' : '';
    let overlayActive = props.isOverlaySidebarActive ? 'overlay-active' : '';

    return (
        <div id="fn-sidebar" className={active + ' ' + overlayActive} onClick={(e) => e.stopPropagation()}>
            { props.isUserInfoLoaded ? <SignInSignOutButton {...props} /> : ""}

            <h5 className="fn-sidebar-header">Overview</h5>
            <div className="list-group">
                <Link to="?page=today" onClick={props.onSidebarButtonClick} className="list-group-item list-group-item-action">
                    📅 Today
                </Link>
                <Link to="?page=agenda" onClick={props.onSidebarButtonClick} className="list-group-item list-group-item-action">
                    🌅 On the agenda
                </Link>
                <Link to="/?page=all" onClick={props.onSidebarButtonClick} className="list-group-item list-group-item-action">
                    📔 All Notes
                </Link>
            </div>
            
            <h5 className="fn-sidebar-header">Tags</h5>
            <TagsList tags={props.tags} location={props.location} onTagClick={props.onTagClick} />
        </div>
    );
}