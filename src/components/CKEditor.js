import React, { Component } from 'react';

import BalloonEditor from 'BalloonEditor';
import GFMDataProcessor from '@ckeditor/ckeditor5-markdown-gfm/src/gfmdataprocessor';
import { attachPlaceholder } from '@ckeditor/ckeditor5-engine/src/view/placeholder'

import { getHashTags, uuid } from '../Utils'

export default class CKEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      instanceId: 'fn-ckeditor-' + uuid(),
    }
  }

  getBlock = () => {
    let content = this.state.editor.getData();

    return {
      tags: getHashTags(content),
      content: content
    }
  }

  onChange = eventInfo => {
    let document = this.state.document;

    // https://stackoverflow.com/questions/45133183/listen-to-event-fired-when-the-content-has-changed-in-ckeditor-5
    if (document.differ.getChanges().length > 0 ) {
      this.props.onEdit && this.props.onEdit(this.props.note, this.getBlock);
    }
  }

  setPlaceholderText = editor => {
    const view = editor.editing.view;
    const viewDoc = view.document;
    const content = viewDoc.getRoot().getChild(0);
    attachPlaceholder(view, content, 'Enter some text...' );
  }

  componentDidMount() {
    BalloonEditor
      .create(document.querySelector('#' + this.state.instanceId), {
        cloudServices: {
          tokenUrl: process.env.REACT_APP_SERVER_BASE_URL + 'token',
          uploadUrl: process.env.REACT_APP_SERVER_BASE_URL + 'api/image/'
        }
      })
      .then(editor => {
        editor.data.processor = new GFMDataProcessor();

        this.setState({
          document: editor.model.document,
          editor: editor,
          initialContent: this.props.content,
        });

        if (this.props.content) {
          editor.setData(this.props.content);
        }

        this.setPlaceholderText(editor);

        editor.model.document.on('change', this.onChange);

        return editor;
      })
      .catch( error => {
          console.error( error );
      });
  }

  shouldComponentUpdate = (nextProps, nextState) => {
    return nextProps.note.etag !== this.props.note.etag;
  }

  componentDidUpdate = (prevProps, prevState) => {
    console.debug("CKEditor componentDidUpdate");

    let data = this.state.editor.getData();

    console.debug("Initial content:");
    console.debug(this.state.initialContent);

    console.debug("Current editor data:");
    console.debug(data);

    console.debug("Incoming data:");
    console.debug(this.props.content);

    let document = this.state.editor.model.document;
    document.off('change', this.onChange);
    
    if (this.state.initialContent === data){
      console.debug("Incoming data has changed. No changes found in editor. Updating editor data.");
      this.state.editor.setData(this.props.content);
      this.setState({ initialContent: this.props.content });
    }
    if (this.state.initialContent !== data) {
      console.error("Incoming data has changed. Changed found in editor. Not updating editor data.")
    }

    document.on('change', this.onChange);
  }

  render() {
    return (
      <div id={this.state.instanceId}></div>
    )
  }
}