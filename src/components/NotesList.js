import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom'
import DatePicker from 'react-datepicker'

import moment from 'moment';

import CKEditor from './CKEditor'
import {QuillEditor, QuillEditorToolbar} from './QuillEditor'
import QuillEditorReadOnly from './QuillEditorReadOnly'
import { buildQueryStringForNote, buildQueryStringForCloseNote, MODE, PAGEQUERY, search, orderByDateAscending, orderByDateDescending, groupByMonth, getPageQueryFromQueryString, getTagQueryFromQueryString } from "../Utils"

import 'react-datepicker/dist/react-datepicker.css';
import './NotesList.css'

class NotesListItem extends Component {
    shouldComponentUpdate(nextProps, nextState) {
        return this.props.note.modifiedTime !== nextProps.note.modifiedTime;
    }

    openNote = e => {
        e.stopPropagation();
        this.props.history.push(buildQueryStringForNote(this.props.location, this.props.note));
        this.props.onClick(this.props.note);
    }

    render = () => {
        console.debug("NotesListItem render ", this.props.note.id);

        let editor = this.props.note.blocks && !this.props.note.blocks[0].type
            ? <div>{this.props.note.blocks[0].content}</div>
            : <QuillEditorReadOnly
                {...this.props.note}
                />;

        let firstChar = this.props.note.blocks && this.props.note.blocks.length && this.props.note.blocks[0].text && this.props.note.blocks[0].text.charAt(0).toUpperCase();

        // 1ED760


        let colorMap = {
            "A" : "#E06055",
            "B" : "#E06055",
            "C" : "#F6BF26",
            "D" : "#9575CD",
            "E" : "#9575CD",
            "F" : "#9575CD",
            "G" : "#4FC3F7",
            "H" : "#4DD0E1",
            "I" : "#4DD0E1",
            "J" : "#57BB8A",
            "K" : "#9CCC65",
            "L" : "#9CCC65",
            "M" : "#EF6C00",
            "N" : "#F6BF26",
            "O" : "#F6BF26",
            "P" : "#F6BF26",
            "Q" : "#F6BF26",
            "R" : "#5E97F6",
            "S" : "#4DD0E1",
            "T" : "#96ceb4",
            "U" : "#689F38",
            "V" : "#B39DDB",
            "W" : "#C2C2C2",
            "X" : "#80DEEA",
            "Y" : "#A1887F",
            "Z" : "#A1887F",
        }
        
        let color = colorMap[firstChar] || "#A3A3A3";

        return (
            <div className="d-flex flex-row" onClick={this.openNote}>
                <div className="fn-note-preview-icon">
                    <span className="fa-stack">
                        <i className="fas fa-circle fa-stack-2x" style={{color}}></i>
                        <span className="fa-stack-1x fa-inverse">{firstChar}</span>
                    </span>
                </div>

                <div className="fn-list-item-content">
                    {editor}
                </div>
            </div>
        );
    }
}

class AgendaTimePicker extends React.Component {
    render () {
        return (
            <button type="button" className="btn btn-link example-custom-input" onClick={this.props.onClick}>
                {this.props.text} &nbsp;<i className="far fa-calendar-alt"></i>
            </button>
        )
    }
}

export class AgendaTimePickerWrapper extends React.Component {
    getText = () => {
        if (!this.props.agendaTime) return '';

        let calendarSettings = {
            lastDay : '[Yesterday]',
            sameDay : '[Today]',
            nextDay : '[Tomorrow]',
            lastWeek : '[last] dddd',
            nextWeek : 'dddd',
            sameElse : 'MMMM Do YYYY'
        };

        return moment(this.props.agendaTime).calendar(null, calendarSettings);
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.note.agendaTime !== nextProps.note.agendaTime;
    }

    getSelectedValue() {
        if (!this.props.agendaTime) return null;

        return moment(this.props.note.agendaTime);
    }

    render() {
        return (
            <DatePicker
                customInput={
                    <AgendaTimePicker
                        text={this.getText()}
                    />
                }
                selected={this.getSelectedValue()}
                onSelect={this.props.onUpdateAgendaTime(this.props.note)}
                popperPlacement="bottom-end"
            />
        )
    }
}

function EditableNotesListItem(props) {
    let toolbarId = "fn-quill-toolbar-" + props.note.id;

    let editor = props.note.blocks && !props.note.blocks[0].type
        ? <CKEditor
            note={props.note}
            content={props.note.blocks[0].content}
            onEdit={props.onEdit}
            /> 
        : <QuillEditor
            note={props.note}
            onEdit={props.onEdit}
            standalone={props.standalone}
            toolbarId={toolbarId}
            atValues={props.atValues}
            hashValues={props.hashValues} 
            autoFocus={true}
            />;

    let toolbar = props.note.blocks && !props.note.blocks[0].type
        ? ""
        : <QuillEditorToolbar id={toolbarId} />

    let closeCurrentNote = () => {
        console.debug("closeCurrentNote");
        props.history.push(buildQueryStringForCloseNote(props.location));
    }

    let stopPropagation = e => {
        e.stopPropagation();
    }

    return (
        <div className="fn-list-item fn-editable-notes-list-item shadow-sm">
            <nav className="navbar sticky-top navbar-light rounded shadow-sm fn-list-item-top-nav" onClick={closeCurrentNote}>
                <div className="navbar-brand" href="#"></div>
                
                <div className="btn-group" role="group" onClick={stopPropagation}>
                    <AgendaTimePickerWrapper note={props.note} agendaTime={props.note.agendaTime} onUpdateAgendaTime={props.onUpdateAgendaTime} />

                    <div className="btn-group" role="group">
                        <button className="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-ellipsis-v"></i>
                        </button>
                        <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a className="dropdown-item" href="#" onClick={props.onDeleteClick(props.note)} >Delete</a>
                        </div>
                    </div>
                </div>
            </nav>

            <div className="fn-list-item-editor flex-column align-items-start list-group-item">
                {editor}
            </div>

            <nav className="navbar navbar-light fn-list-item-bottom-nav fn-sticky-bottom">
                {toolbar}
                {/* <NoteMetadata {...props} /> */}
            </nav>
        </div>
        
    )
}

export function NotesListGroup(props) {
    console.debug("NotesListGroup render", props.notes);

    return (
        props.notes.map((note) =>
            <div key={note.id} className={props.currentNoteId !== note.id ? "fn-list-item flex-column align-items-start list-group-item list-group-item-action" : ""}>
                {props.currentNoteId === note.id
                    ? <EditableNotesListItem
                        history={props.history}
                        location={props.location}
                        note={note}
                        currentNoteId={props.currentNoteId}
                        onClick={props.onNotesListItemClick}
                        onEdit={props.onEdit}
                        atValues={props.atValues}
                        hashValues={props.hashValues}
                        onUpdateAgendaTime={props.onUpdateAgendaTime}
                        onDeleteClick={props.onDeleteClick}  />
                    : <NotesListItem
                        history={props.history}
                        location={props.location}
                        note={note}
                        currentNoteId={props.currentNoteId}
                        onClick={props.onNotesListItemClick} />
                }
            </div>
        )
    );
}

export class TodayEmptyState extends PureComponent {
    render = () => {
        return (
            <div class="container d-flex empty-state-container">
                <div class="align-self-center">
                    <div>
                        <img className="mx-auto d-block" src="static/images/empty/clapping-hands.png" alt="" />
                        <h4 className="text-center mt-3">Congratulations!<br/>Your agenda is clear for the day.</h4>
                    </div>

                    <div class="alert alert-primary fade show mt-3" role="alert">
                        <h5>Want to add something to today's agenda?</h5>
                        
                        Once you've created a note, use the calendar icon, and select today's date.
                        <div>
                            <img class="img-fluid" src="static/images/how-to-add-to-the-agenda.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export class AgendaEmptyState extends PureComponent {
    render = () => {
        return (
            <div class="container d-flex empty-state-container">
                <div class="align-self-center">
                    <div>
                        <img className="mx-auto d-block" src="static/images/empty/lightbulb.png" alt="" />
                        <h4 className="text-center mt-3">Your agenda is empty.<br/>Let's add something, and start planning for the week ahead.</h4>
                    </div>

                    <div class="alert alert-primary fade show mt-3" role="alert">
                        <h5>Want to add something to the agenda?</h5>
                        
                        Once you've created a note, use the calendar icon, and select a date.
                        <div>
                            <img class="img-fluid" src="static/images/how-to-add-to-the-agenda.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export class NotesList extends PureComponent {
    static propTypes = {
        notesListGroupedItems: PropTypes.object.isRequired,
    }

    render = () => {
        console.debug("NotesList render");

        let monthKeys = Array.from(this.props.notesListGroupedItems.keys());

        let pageQuery = getPageQueryFromQueryString(this.props.location);

        if (!this.props.notesListGroupedItems.size && (pageQuery === PAGEQUERY.AGENDA || pageQuery === PAGEQUERY.TODAY)) {
            return (
                pageQuery === PAGEQUERY.TODAY
                    ? <TodayEmptyState pageQuery={pageQuery} />
                    : <AgendaEmptyState pageQuery={pageQuery} />
            )
        }

        return (
            monthKeys.map((monthKey) => 
                <div key={monthKey}>
                    <div className='fn-list-item'>
                        <div class="fn-list-group-title">{this.props.pageQuery !== PAGEQUERY.TODAY ? monthKey : ''}</div>    
                    </div>
                    <div className="fn-notes-list list-group">
                        <NotesListGroup {...this.props} notes={this.props.notesListGroupedItems.get(monthKey)} />
                    </div>
                </div>
            )
        );
    }
}