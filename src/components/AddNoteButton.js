import React from 'react';
import { Link } from 'react-router-dom'

import './AddNoteButton.css';

export default function AddNoteButton(props) {
    return (
        <Link className="fn-add-note-button-container text-center" to={props.addNoteUrl} onClick={props.onAddNoteClick}>
            <img src="//ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/1x/btw_ic_speeddial_white_24dp.png"
                srcSet="//ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/2x/btw_ic_speeddial_white_24dp_2x.png 2x" alt="" aria-hidden="true" />
        </Link>
    );
}