import React from 'react';
import { Link } from 'react-router-dom'

import { getPageQueryFromQueryString, getTagQueryFromQueryString, PAGEQUERY } from '../Utils'

function getTitle(location) {
    let tagQuery = getTagQueryFromQueryString(location);

    if (tagQuery) {
        return tagQuery;
    }

    let pageQuery = getPageQueryFromQueryString(location);

    if (pageQuery === PAGEQUERY.TODAY) {
        return "Today";
    }
    
    if (pageQuery === PAGEQUERY.AGENDA) {
        return "On the Agenda";
    }

    return "All Notes";
}

export default function ListModeTopNav(props) {
    let title = getTitle(props.location);

    return (
        <div className='row fn-top-nav-wrapper'>
            <nav className="fn-top-nav fixed-top navbar d-block">
                <div className="btn-toolbar justify-content-between">
                    <div className="btn-group">
                        <button type="button" onClick={props.onBarsClick} id="sidebarCollapse" className="btn btn-link"><i className="fas fa-bars"></i></button>
                        {/* <Link to={props.addNoteUrl}>
                            <button type="button" onClick={props.onAddNoteClick} className="btn btn-lg btn-link" data-toggle="tooltip" data-placement="bottom" title="Create a new note"><i className="fas fa-plus-square"></i></button>
                        </Link> */}
                    </div>
                    <div className="navbar-brand">{title}</div>
                    <div className="btn-group">
                        {/* <button className="btn btn-link my-2 my-sm-0" type="submit"><i className="fas fa-sync"></i></button>
                        <button type="button" className="btn btn-link"><i className="fas fa-info"></i></button>
                        <button type="button" className="btn btn-link"><i className="fas fa-question-circle"></i></button> */}
                        {/* <Link to={props.searchModeUrl} className="d-block d-xl-none">
                            <button type="button" onClick={props.onSearchClick} className="btn btn-link"><i className="fas fa-search"></i></button>
                        </Link> */}
                    </div>
                </div>
            </nav>
        </div>
    );
}