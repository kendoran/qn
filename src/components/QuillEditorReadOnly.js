import React, { Component } from 'react';
import Quill from 'quill'

import { uuid } from '../Utils'

import './quill.snow.css';
import './QuillEditor.css';

export default class QuillEditorReadOnly extends Component {
    constructor(props) {
      super(props);
  
      this.state = {
        instanceId: "fn-quill-" + uuid(),
      }
    }
  
    initContent = (editor) => {
      if (!this.props.blocks) return null;
  
      let block = this.props.blocks[0];
  
      if (block.type === 'quill') {
        block.content && editor.setContents(block.content);
      }
      else if (block.type === 'quill-conflict') {
        editor.pasteHTML("<b>A conflict was found. Resolving conflicts isn't supported yet. This #fn/conflict note was created to avoid losing changes.</b>");
      }
      else {
        console.error("Unrecognised block", block, this.props.note);
      }
    }
  
    componentDidMount = () => {
      var options = {
          modules: {
            toolbar: null,
            syntax: true,
          },
          readOnly: true,
          theme: 'snow'
        };
  
      var editor = new Quill('#' + this.state.instanceId, options);
      this.setState({editor})
      this.initContent(editor);
    }
  
    componentWillUnmount() {
      this.setState({editor : null})
    }
  
    shouldComponentUpdate(nextProps, nextState) {
      return this.props.modifiedTime !== nextProps.modifiedTime;
    }
  
    componentDidUpdate = (prevProps, prevState) => {
      let block = this.props.blocks && this.props.blocks[0];
  
      if (block.type === 'quill') {
        this.state.editor.setContents(block.content);
      }
    }
  
    render() {
        console.debug("QuillEditorReadOnly render " + this.props.id);
  
        return (
          <div id={this.state.instanceId} ref="quillElement"></div>
        )
      }
  }