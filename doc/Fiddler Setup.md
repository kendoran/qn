At the end of 'OnBeforeRequest'

```
        if (oSession.host.toLowerCase() == "localhost:8040") {
            if (oSession.PathAndQuery == "/") oSession.host = "localhost:3000";
            else if (oSession.fullUrl.indexOf("/?")>-1) oSession.host = "localhost:3000";
            else if (oSession.fullUrl.indexOf("static/")>-1) oSession.host = "localhost:3000";
            else if (oSession.fullUrl.indexOf("webpack")>-1) oSession.host = "localhost:3000";
            else if (oSession.fullUrl.indexOf("sockjs")>-1) oSession.host = "localhost:3000";
            else if (oSession.fullUrl.indexOf("favicon/")>-1) oSession.host = "localhost:3000";
            else oSession.host = "localhost:5000";
        }
```