# AWS InvalidCypherTextException when reading an encrypted DynamoDB table which has been restored from a backup

If you attempt to read encrypted data from a DynamoDB table which has been restored from a backup, you may see the following errors:

```
InvalidCiphertextException: An error occurred (InvalidCiphertextException) when calling the Decrypt operation
```

```
UnwrappingError: Failed to unwrap AWS KMS protected materials
```

In this case:
* A backup of the DynamoDB table "Notes" had been restored to a new DynamoDB table "Notes-Restored"
* The table was using encryption at rest
* One of the columns of the DynamoDB table had been encrypted using a CMK.
* I was using the [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html) DynamoDB client, and [dynamodb-encryption-sdk](https://aws-dynamodb-encryption-python.readthedocs.io/en/latest/lib/encrypted/client.html)


TLDR; the restored DynamoDB table must have the same name as the original DynamoDB table, and be restored to the same account that it was originally created in.

More details below

....

In its requests to AWS KMS, DynamoDB uses an encryption context with two key–value pairs.

```
{
    "aws:dynamodb:tableName": "<table-name>"
    "aws:dynamodb:subscriberId": "<account-id>"
}
```

i.e. if your original table name was 'Notes', and the account id that the table was created in is '12345678', the encryption context would look like:

```
{
    "aws:dynamodb:tableName": "Notes"
    "aws:dynamodb:subscriberId": "12345678"
}
```

Then if you restored a backup to a table such as 'Notes-Restored', the encryption context would look like:

```
{
    "aws:dynamodb:tableName": "Notes-Restored"
    "aws:dynamodb:subscriberId": "12345678"
}
```

The encryption context would no longer match the encryption context which was originally used to encrypt the data, resulting in an UnwrappingError or InvalidCiphertextException.

Restore the database to a table with the same name as the original table. This will ensure that the encryption context matches the original.

More details on the DynamoDB encryption context here: https://docs.aws.amazon.com/kms/latest/developerguide/services-dynamodb.html#dynamodb-encryption-context

## Is there a better way?

Restoring to a table with the same name as the original only worked for me because I'd deleted the original table.

If the original table still exists, you'll need to explore other options.

There may be a way to provide the encryption context, specifying the original table name. If you know how, or have any other suggestions, feel free to leave a comment below.







https://docs.aws.amazon.com/kms/latest/developerguide/services-dynamodb.html#dynamodb-encryption-context

https://aws-dynamodb-encryption-python.readthedocs.io/en/latest/lib/encrypted/client.html

https://aws-dynamodb-encryption-python.readthedocs.io/en/latest/lib/materials_providers/aws_kms.html?highlight=context

https://aws-dynamodb-encryption-python.readthedocs.io/en/latest/lib/encrypted/client.html?highlight=encryptedclient#dynamodb_encryption_sdk.encrypted.client.EncryptedClient

https://docs.aws.amazon.com/dynamodb-encryption-client/latest/devguide/python-examples.html

https://github.com/aws/aws-dynamodb-encryption-java/issues/28

https://github.com/awsdocs/aws-dynamodb-encryption-docs/blob/master/doc_source/direct-kms-provider.md

https://github.com/aws/aws-dynamodb-encryption-python

AwsKmsCryptographicMaterialsProvider is direct https://docs.aws.amazon.com/dynamodb-encryption-client/latest/devguide/direct-kms-provider.html

EncryptedClient: https://github.com/aws/aws-dynamodb-encryption-python/blob/master/src/dynamodb_encryption_sdk/encrypted/client.py

crypto_config_from_cache: https://github.com/aws/aws-dynamodb-encryption-python/blob/80465a4b62b1198d519b0ba96da1643eb8af2c9a/src/dynamodb_encryption_sdk/internal/utils.py#L152

Successful:

{
    "eventVersion": "1.05",
    "userIdentity": {
        "type": "IAMUser",
        "principalId": "AIDAI24YMIQRJW232XCHM",
        "arn": "arn:aws:iam::466360564464:user/matt",
        "accountId": "466360564464",
        "accessKeyId": "AKIAJW2G2J3YWPCOPGHQ",
        "userName": "matt"
    },
    "eventTime": "2018-12-02T09:47:45Z",
    "eventSource": "kms.amazonaws.com",
    "eventName": "Decrypt",
    "awsRegion": "ap-southeast-2",
    "sourceIPAddress": "210.246.29.115",
    "userAgent": "Boto3/1.7.48 Python/3.6.3 Windows/10 Botocore/1.10.63 DynamodbEncryptionSdkPython/1.0.4",
    "requestParameters": {
        "encryptionContext": {
            "*amzn-ddb-sig-alg*": "HmacSHA256/256",
            "*amzn-ddb-env-alg*": "AES/256",
            "id": "d930cbb1-ff10-4651-bd44-5395584ccadf",
            "*aws-kms-table*": "FluentNotesCache-1"
        }
    },
    "responseElements": null,
    "requestID": "40d1751c-bda9-4f87-857e-8edaffc7bf60",
    "eventID": "8d26c061-9aaf-4dcd-be46-68f0a5797be8",
    "readOnly": true,
    "resources": [
        {
            "ARN": "arn:aws:kms:ap-southeast-2:466360564464:key/523f2977-6268-421d-9005-ec35f1d1461e",
            "accountId": "466360564464",
            "type": "AWS::KMS::Key"
        }
    ],
    "eventType": "AwsApiCall",
    "recipientAccountId": "466360564464"
}

Failure:

{
    "eventVersion": "1.05",
    "userIdentity": {
        "type": "IAMUser",
        "principalId": "AIDAI24YMIQRJW232XCHM",
        "arn": "arn:aws:iam::466360564464:user/matt",
        "accountId": "466360564464",
        "accessKeyId": "AKIAJW2G2J3YWPCOPGHQ",
        "userName": "matt"
    },
    "eventTime": "2018-12-04T01:25:18Z",
    "eventSource": "kms.amazonaws.com",
    "eventName": "Decrypt",
    "awsRegion": "ap-southeast-2",
    "sourceIPAddress": "210.246.29.115",
    "userAgent": "Boto3/1.7.48 Python/3.6.3 Windows/10 Botocore/1.10.63 DynamodbEncryptionSdkPython/1.0.4",
    "errorCode": "InvalidCiphertextException",
    "requestParameters": {
        "encryptionContext": {
            "*amzn-ddb-sig-alg*": "HmacSHA256/256",
            "*amzn-ddb-env-alg*": "AES/256",
            "*aws-kms-table*": "FluentNotesRestored-1"
        }
    },
    "responseElements": null,
    "requestID": "e89d68ab-1506-4478-8cb0-eabd27b3f759",
    "eventID": "8e304e79-f0c8-40d2-95fe-d7866ba6c6ce",
    "readOnly": true,
    "resources": [
        {
            "ARN": "arn:aws:kms:ap-southeast-2:466360564464:key/523f2977-6268-421d-9005-ec35f1d1461e",
            "accountId": "466360564464",
            "type": "AWS::KMS::Key"
        }
    ],
    "eventType": "AwsApiCall",
    "recipientAccountId": "466360564464"
}