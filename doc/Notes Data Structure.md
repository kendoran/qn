## Goals

### Data Storage
One of the things that's important to this app is that user data is stored on their own storage which they have control over; if they one day decide to stop using my app, they can keep their data, and revoke the app's permission to access their google drive storage.

Because of this, I don't want to store user data on a central database.

Initially the data store will be the user's personal Google Drive, which will be the source of truth for the note data.

For performance and reliability reasons, it may be necessary to temporaily cache data in a central store.

Any temporarily cached data must be automatically deleted from the cache when it's no longer needed. This could be done via a rolling expiry i.e. if it's not accessed in the last hour, and the any necessary changes have been written to the user data store, delete from the cache.

### App must work offline
The user updates notes on their mobile, or in the web browser while they don't have connectivity, and the notes should be synchronised later.

## Data volume and the average size of a note

Imagine a user who has used the app over few months, and has 1000 notes.

These notes range from the size of a tweet (20 words, roughly 200 bytes), to a page of text (600 words, roughly 3KB).

Since the user joined a few months ago, they published 800 tweet size notes, and 200 page size notes. This is roughly 760kb of content.

## Google drive observations

### Latency

There are a couple of seconds latency on average to fetch a file from Google Drive. (TODO: more data, references?)

The Google Drive Python API does not support bulk fetching of files. This means that I can't pass in a range of file ids, and get a set of files back in a single response.

There is also rate limiting, if I were to store notes as individual files on google drive, and fire off hundreds of requests for a user, and I had many users, I'd quickly run into this limitation.

Because of this, it's not practical to store individual notes as individual files in Google Drive.

### Writing to Google Drive (spoiler: last write wins)

Google drive doesn't support any kind of file locking, or partial updates, which means that the most recent write to the file determines the file content.

Imagine two processes are operating on the same file in Google Drive which contains two notes:
1. Process 1 loads file xyz.json containing notes: ("foo", "bar")
2. Process 2 loads file xyz.json containing notes: ("foo", "bar")
3. Process 1 updates note 1 with the text "foo updated"
4. Process 2 updates note 2 with the text "bar updated"
5. Process 1 saves file xyz.json with notes:  ("foo updated", "bar")
6. Process 2 saves file xyz.json with notes:  ("foo", "bar updated")

At the end, xyz.json in Google Drive contains ("foo", "bar updated") if Process 2 completed last or ("foo updated", "bar") if Process 1 completed last. In either case, data is lost.

There is another case where both processes update note 1 in xyz.json. For this we need some kind of operational transform. Details TBC.

To avoid these problems we'll need an intemediary server which will handle any locking needed, and aggregate the data from multiple requests, and write to Google Drive.

## Note file format in Google Drive (and later, Dropbox / iCloud / etc)

A note file can store many files. It takes a couple of seconds on average to fetch a file from Google Drive.

Because of the average note size, and the latency when fetching from Google Drive, it would be impractical to store and fetch a large number of individual files.

would be impractial to fetch 1000 files from google drive, which were betwen a few bytes and 1k or so in size. a file format that
can store many notes is a design decision.

{
	"notes": [
		{
			"id": "cea6ffa177694c6e945d93265f580756",
			"createdTime": "2018-06-21T10:26:09.357Z",
			"modifiedTime": "2018-06-21T10:26:09.357Z",
			"etag": "abca34adf35asdfhaadfacww",
			"sharedWith": [
				{ "email": "foo@bar.com", "id": "abcd1234", "shareTime": "2018-06-21T10:26:09.357Z" }
			],
			"reminders": [
				{ "type": "day", "date": "2018-06-21T10:26:09.357Z" },
				{ "type": "weekly", "startDate": "2018-06-21T10:26:09.357Z" },
				{ "type": "spacedRepetition", "mode": "xyz", "startDate": "2018-06-21T10:26:09.357Z" }
			],
			"blocks": [
				{
					"contentType": "text/markdown",
					"content": "Foo Bar Baz",
				},
				{
					"type": "latex"
					"content": "latex formula",
					"archived": true,
				},
				{
					"type": "html-table"
					"content": "html table",
				},
				{
					"type": "code",
					"language": "python",
					"content": "preformatted code block",
				}
				{
					"type": "todo",
					"contentType": "text/html",
					"content": "todo content",			
				},
				{
					"type": "sketch/handwritten"
					"content": "binary data",
				},
				{
					"type": "svg"
					"content": "svg data",
				},
			]
		}
	]

}

### etag

The entire note could be hashed for the etag.

### sharedWith

Some users (i.e Mitch) want to be able to share their notes with other people.

I'm not sure how this will be handled via Google Drive. This could be a case where shared notes are stored on a central DB. Details TBC.

### reminders

Some notes might need a follow-up on a certain day (need to do this on Tuesday), or a weekly review (need to remember this)

Reminders could have a "followups" block which track whether the reminder was followed up on, or complete. Too early to design this particular thing.

### blocks

An ordered array of content blocks.

Having all note content in CKEditor might not be practical if the user wants to mix content. i.e. Latex formulas are not currently supported in CKEditor.

It should be possible to have a note made up of:
* Markdown content
* Latex formula (Default is renered mode, click to switch to edit mode... rendering formulas is not possible in the current CKEditor, which is why I'm thinking of a separate block)
* HTML table - there are good table editors out there, just use them for a HTML table block. Similar to the Latex block, CKEditor does not currently support editing tables.
* Todo list - I wonder if it's worth making "content" a string or an object in certain cases. Maybe the user would want reminders against particular todo items.
* Code block

The content blocks can be managed via plugins. The default plugin is CKEditor. An alternative plugin can be specified by the "type" property.

contentType is needed for determining whether CKEditor needs to run in Markdown or HTML mode. For code or todo blocks, contentType is implicit.

The user should be able to remove a content block, without deleting it. On remove, the block is intially archived. A further delete step would remove the note entirely.

Support for reminders on notes. I.e. an action item needs to happen on Tuesday, i.e. "follow up with xyz"

## Using DynamoDB as a cache for performance and reliability

## Why DynamoDB?

Free tier
Supports data expiry
Can function as a cachhe

All the jobs seem to be using AWS


Can store JSON documents
Queryable

### Storing data in DynamoDB

All DynamoDB data should have a rolling expiry; if it's not accessed within the last i.e. hour, the DynamoDB data is deleted.

When a note is being operated on, check to see whether the file containing the note has already been loaded into DynamoDB. If not, load the file into DynamoDB.

As part of loading into DynamoDB the value of the ExpirationTime property for i.e. 2 hours in the future.

When accessed, check ExpirationTime, and if it's within x minutes of expiry, update the Expiration time.

While the contents of a file is in DynamoDB, DynamoDB is the source of truth for the note file.

Individual notes in a file are stored as individual rows in DynamoDB with the following high-level schema: (see: dynamodb-table.png)

When saving a file back to Google Drive, bundle up the individual notes that make up the file, and write the entire file at once.

Store Google Drive "List Files" result in DynamoDB. Use a different table to the notes. Set a "pendingWrite" flag on the file, if it needs to be written to.

Store user's google tokens in DynamoDB, keyed to user id.

## Fetching and updating note data

When fetching a note, fetch from DynamoDB.

After an update to a note, the DynamoDB data for a note file is aggreagated, and saved to the appropriate file in Google Drive.

This could potentially be an offline process. The user could be updated via push notifications, rather than synchronous responses.

IDEA: If there existed a google drive equivalent to DynamoDB, the app could use that instead.

Consider a sync interval; RemoteStorage could spam the server with a lot of updates all at once. Run Sync every i.e. 30 seconds, so that the server has a chance to receive a bunch of updates, and save once, rather than a save for each update. Consider AWS Lambda task that automatically runs every 30 secs (per user?) to write any files with the pendingWrite flag.

Use SQS with de-duplication https://aws.amazon.com/blogs/aws/new-for-amazon-simple-queue-service-fifo-queues-with-exactly-once-delivery-deduplication/

i.e. When a user updates a note, send update request to SQS with MessageDeduplicationId as the file id.
{ "MessageDeduplicationId": "file_id", "UserId": "user_id" }

def sync(user_id, file_id):
	make sure that the SQS message is removed from the queue
	fetch google auth tokens for user from dynamodb
	fetch latest for file_id from dynamodb
	assemble file contents for file_id
	write entire file to google drive

## Updating the users device (mobile/web)

RemoteStorage takes care of this via the endpoint which returns etags etc.

RemoteStorage fetches files one-by-one which may not be ideal for 1000 files. Find a way to bulk load data into the RemoteStorage browser cache if necessary.

I might be storing note state in the react/redux state. This is different to RemoteStorage state.

On react state updated via the user -> update remote storage
On remote storage state updated via a sync operation -> update react state








