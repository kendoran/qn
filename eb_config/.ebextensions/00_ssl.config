Resources:
    sslSecurityGroupIngress:
        Type: AWS::EC2::SecurityGroupIngress
        Properties:
            GroupId: {"Fn::GetAtt" : ["AWSEBSecurityGroup", "GroupId"]}
            IpProtocol: tcp
            ToPort: 443
            FromPort: 443
            CidrIp: 0.0.0.0/0

files:
    /etc/httpd/conf.d/ssl.pre:
        mode: "000644"
        owner: root
        group: root
        content: |
            LoadModule ssl_module modules/mod_ssl.so
            Listen 443

            <VirtualHost *:443>
                <Directory /opt/python/current/app/build/static>
                    Order deny,allow
                    Allow from all
                </Directory>
                
                SSLEngine on
                SSLCertificateFile "/etc/letsencrypt/live/fn-prod-1.us-west-2.elasticbeanstalk.com/fullchain.pem"
                SSLCertificateKeyFile "/etc/letsencrypt/live/fn-prod-1.us-west-2.elasticbeanstalk.com/privkey.pem"
                SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
                SSLProtocol All -SSLv2 -SSLv3
                SSLHonorCipherOrder On
                SSLSessionTickets Off
                
                Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains; preload"
                Header always set X-Frame-Options DENY
                Header always set X-Content-Type-Options nosniff
                
                ProxyPass / http://localhost:80/ retry=0
                ProxyPassReverse / http://localhost:80/
                ProxyPreserveHost on
                RequestHeader set X-Forwarded-Proto "https" early
                # If you have pages that may take awhile to
                # respond, add a ProxyTimeout:
                # ProxyTimeout seconds
            </VirtualHost>

    /etc/cron.d/renew_cert:
        mode: "000777"
        owner: root
        group: root
        content: |
            # renew Let's Encrypt cert with certbot command
            0 1,13 * * * /usr/local/bin/certbot-auto renew

packages:
    yum:
        epel-release: []
        mod24_ssl : []

# Steps here
# 1. Install certbot
# 2. Get cert (stop apache before grabbing)
# 3. Link certs where Apache can grab
# 4. Get the Apache config in place
# 5. Move certbot-auto into /usr/local/bin
container_commands:
    10_installcertbot:
        command: "wget https://dl.eff.org/certbot-auto;chmod a+x certbot-auto"
    20_getcert:
        command: "sudo ./certbot-auto certonly --debug --non-interactive --email matt@mattbutton.com --agree-tos --debug --apache --domains fn-prod-1.us-west-2.elasticbeanstalk.com --keep-until-expiring"
    30_link:
        command: "sudo ln -sf /etc/letsencrypt/live/fn-prod-1.us-west-2.elasticbeanstalk.com /etc/letsencrypt/live/ebcert"
    40_config:
        command: "sudo mv /etc/httpd/conf.d/ssl.pre /etc/httpd/conf.d/ssl.conf"
    50_mv_certbot_to_temp_for_cron_renew:
        command: "sudo mv ./certbot-auto /usr/local/bin"