#pylint: disable=C0301, C0111, E0401, C0103
import copy
import hashlib
import json
import tempfile

import dateutil.parser
import flask
from flask import request

import dynamocache
import gdrive

from settings import SETTINGS
from fn_utils import fn_json_dumps, no_cache
from fn_auth import AUTH_TOKEN_KEY, build_credentials, login_required

api = flask.Blueprint('rs_api', __name__)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def build_etag(note):
    note_copy = copy.copy(note)

    if 'etag' in note_copy:
        del note_copy['etag']

    content = json.dumps(note_copy)
    encoded_content = content.encode('utf-8')

    return hashlib.md5(encoded_content).hexdigest()

def build_folder_description(all_notes, content_type='application/json'):
    folder_items = {}

    for item in all_notes:
        file_key = item['id']
        folder_items[file_key] = {
            'ETag': item['etag'],
            'Content-Type': content_type,
            'Last-Modified': get_http_date(item['modifiedTime'])
        }

    return {
        "@context": "http://remotestorage.io/spec/folder-description",
        "items": folder_items
    }

def build_images_folder_description(all_images):
    folder_items = {}

    for item in all_images:
        file_key = item['filename']
        folder_items[file_key] = {
            'ETag': item['etag'],
            'Content-Length': item['contentLength'],
            'Content-Type': item['contentType'],
            'Last-Modified': get_http_date(item['modifiedTime'])
        }

    return {
        "@context": "http://remotestorage.io/spec/folder-description",
        "items": folder_items
    }

def etags_match(header_key, etag):
    if header_key in flask.request.headers:
        if_none_match = flask.request.headers[header_key].replace("\"", "").replace("W/", "")
        return if_none_match == etag

    return False

def get_http_date(isodate):
    parsed = dateutil.parser.parse(isodate)
    return parsed.strftime('%a, %d %b %Y %H:%M:%S %Z')

#============================
# Remote Storage
#
# https://tools.ietf.org/id/draft-dejong-remotestorage-11.txt
# https://github.com/remotestorage/remotestorage.js/
# https://remotestoragejs.readthedocs.io/en/latest/
#============================

RS_AUTH_COMPLETE_REDIRECT_BASE_URL = SETTINGS.BASE_URI + "#"

@api.route("/.well-known/webfinger", methods=['GET'])
@api.route("/.well-known/host-meta", methods=['GET'])
@api.route("/.well-known/host-meta.json", methods=['GET'])
@no_cache
def rs_web_finger():
    response_data = {
        "links": [{
            "rel": "http://tools.ietf.org/id/draft-dejong-remotestorage",
            "href": '/rs',
            "properties": {
                "http://remotestorage.io/spec/version": "draft-dejong-remotestorage-10",
                "http://tools.ietf.org/html/rfc6749#section-4.2": '/rs/auth',
                "http://tools.ietf.org/html/rfc6750#section-2.3": None,
                "http://tools.ietf.org/html/rfc7233": None,
                "http://remotestorage.io/spec/web-authoring": None
            }
        }]
    }

    return flask.Response(fn_json_dumps(response_data), 200, {}, "application/json")

@api.route("/rs/auth", methods=['GET'])
@login_required
@no_cache
def rs_oauth():
    #client_id = flask.request.args.get('client_id', default=None, type=None) # i.e. https://litewrite.net/
    #redirect_uri = flask.request.args.get('redirect_uri', default=None, type=None) # i.e. https://litewrite.net/
    #response_type = flask.request.args.get('response_type', default=None, type=None) # i.e. token
    #rs_scope = flask.request.args.get('scope', default=None, type=None) # i.e. documents:rw
    #state = flask.request.args.get('state', default=None, type=None) # i.e. !(97e5df67-50f6-44ed-b51a-68e698aeb904)-litewrite

    # Interesting point: state in auth response was i.e. &state=!(19f5b6cb-4158-488b-8191-a11027f2a7f4)-fn in litewrite url.
    # In this case, state is just a reference to the document,
    # and varies when you click on other documents. Bookmarkable doc references, not oauth state

    final_url = RS_AUTH_COMPLETE_REDIRECT_BASE_URL + "access_token=" + flask.session[AUTH_TOKEN_KEY]['access_token']

    return flask.redirect(final_url, code=302)

@api.route("/rs/oauth/<user_id>", methods=['GET'])
@no_cache
def rs_href():
    return None

# TODO: Move to gdrive
@api.route("/api/gdrive/load", methods=['GET']) # TODO: Should be a POST
@login_required
@no_cache
def load_from_gdrive():
    credentials = build_credentials()

    file_id = gdrive.get_active_file_id(credentials)
    gdrive_notes_raw = gdrive.get_file_contents(credentials, file_id)
    gdrive_parsed = json.loads(gdrive_notes_raw)

    if 'notes' in gdrive_parsed:
        for note in gdrive_parsed['notes']:
            current_note = dynamocache.get_note(note['id'])

            if current_note is None:
                note['fileId'] = file_id
                dynamocache.put_note(note)
            else:
                current_note['fileId'] = file_id # Ensure that fileId is set correctly!
                dynamocache.put_note(current_note)

    return flask.Response('Done', 200)

# Do I need to check if-none-match and return 304 (not modified)
@api.route("/rs/notes/", methods=['GET'])
@login_required
def rs_get_folder_description():
    credentials = build_credentials()
    active_file_id = gdrive.get_active_file_id(credentials)
    all_notes = dynamocache.get_folder_description_for_file(active_file_id)

    if not all_notes:
        load_from_gdrive()
        all_notes = dynamocache.get_folder_description_for_file(active_file_id)

    response_data = build_folder_description(all_notes)
    headers = {
        'Cache-Control': 'no-cache'
    }

    return flask.Response(fn_json_dumps(response_data), 200, headers, "application/ld+json")

# https://github.com/remotestorage/remotestorage.js/blob/e65250bd38c713ef59853832c69313987cf6dfa5/src/googledrive.js#L259
@api.route("/rs/notes/<note_id>", methods=['GET'])
@login_required
def rs_get_note(note_id):
    headers = {}
    note = dynamocache.get_note(note_id)

    if note:
        headers = {
            'ETag': '"' + note['etag'] + '"',
            'Cache-Control': 'no-cache'
        }
    else:
        headers = {}

    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/If-None-Match
    if etags_match('If-None-Match', note['etag']):
        return flask.Response('', 304, headers)

    if note:
        return flask.Response(fn_json_dumps(note), 200, headers, 'application/json')
    else:
        return flask.Response('', 404)

# https://github.com/remotestorage/remotestorage.js/blob/e65250bd38c713ef59853832c69313987cf6dfa5/src/googledrive.js#L259
@api.route("/rs/notes/<note_id>", methods=['PUT'])
@login_required
def rs_store_note(note_id):
    existing_note = dynamocache.get_note(note_id)

    if existing_note:
        if 'If-Match' in request.headers:
            if not etags_match('If-Match', existing_note['etag']):
                return flask.Response('', 412, {})

    credentials = build_credentials()
    file_id = gdrive.get_active_file_id(credentials)

    note = {
        'id': request.json['id'],
        'fileId': file_id,
        'createdTime': request.json['createdTime'],
        'modifiedTime': request.json['modifiedTime'],
        'agendaTime': request.json['agendaTime'] if 'agendaTime' in request.json else None,
        'fn-status': dynamocache.NOTE_STATUS_MODIFIED,
        'blocks': request.json['blocks']
    }

    note['etag'] = build_etag(note)

    dynamocache.put_note(note)

    if existing_note:
        response_code = 200
    else:
        response_code = 201

    headers = {
        'ETag': '"' + note['etag'] + '"',
        'Cache-Control': 'no-cache'
    }

    return flask.Response('', response_code, headers, 'application/json')

@api.route("/rs/notes/<note_id>", methods=['DELETE'])
@login_required
def rs_delete_note(note_id):
    existing_note = dynamocache.get_note(note_id)

    if existing_note:
        existing_note['fn-status'] = dynamocache.NOTE_STATUS_DELETED
        existing_note['etag'] = build_etag(existing_note)

        dynamocache.put_note(existing_note)

    return flask.Response('', 200, {})

@api.route('/rs/images/')
@login_required
def rs_list_images():
    credentials = build_credentials()
    gdrive_images = gdrive.get_images_list(credentials)

    images = []

    for gdrive_image in gdrive_images:
        image = {
            'filename': gdrive_image['name'],
            'etag': gdrive_image['md5Checksum'],
            'contentLength': gdrive_image['size'],
            'contentType': gdrive_image['mimeType'],
            'modifiedTime': gdrive_image['modifiedTime']
        }

        images.append(image)

    folder_desc = build_images_folder_description(images)
    headers = {
        'Cache-Control': 'no-cache'
    }

    return flask.Response(fn_json_dumps(folder_desc), 200, headers, "application/ld+json")

@api.route('/rs/images/<name>.<ext>', methods=['GET'])
@login_required
def rs_get_image(name, ext):
    file_name = name + '.' + ext

    credentials = build_credentials()

    images = gdrive.get_images_list(credentials)
    matching_images = list(filter(lambda image: image['name'] == file_name, images))
    image_id = matching_images[0]['id']

    metadata = gdrive.get_image_metadata(credentials, image_id)

    if etags_match('If-None-Match', metadata['md5Checksum']):
        headers = {
            'ETag': '"' + metadata['md5Checksum'] + '"',
            'Cache-Control': 'no-cache'
        }
        return flask.Response('', 304, headers)

    fh = gdrive.get_image_stream(credentials, image_id)

    response = flask.make_response(flask.send_file(
        fh,
        metadata['mimeType'] + '; charset=binary'
    ))

    response.headers['ETag'] = metadata['md5Checksum']
    response.headers['Content-Length'] = metadata['size']
    response.headers['Cache-Control'] = 'no-cache'

    return response

@api.route('/rs/images/<name>.<ext>', methods=['PUT'])
@login_required
def rs_store_image(name, ext):
    credentials = build_credentials()

    fp = tempfile.TemporaryFile()
    ch = flask.request.stream.read()
    fp.write(ch)
    fp.seek(0)

    file_name = name + '.' + ext
    mime_type = flask.request.headers['Content-Type'] #.split(';')[0]
    file_id = gdrive.save_image(credentials, file_name, mime_type, fp)

    headers = {
        'ETag': file_id,
        'Cache-Control': 'no-cache'
    }

    return flask.Response('', 201, headers, 'application/json')
