#pylint: disable=C0301, C0111, E0401, W0212

import json
import unittest

from decimal import Decimal

import dynamocache

FN_SAMPLE_NOTE={ 
    "id":"f6a45e14-6c64-46cf-b23a-171accd84a15",
    "contentUrl":"http://localhost:8040/note/content",
    "createdTime":"2018-08-02T10:30:20.219Z",
    "modifiedTime":"2018-08-02T10:30:27.319Z",
    "tags":[],
    "blocks":[{
        "type":"quill-delta",
        "tags":[],
        "content":{"ops":[{"insert":"xyz\n"}]}
    }],
    "@context":"http://remotestorage.io/spec/modules/notes/note"
}

class TestStringMethods(unittest.TestCase):
    def test_serialize(self):
        dynamo_type = dynamocache._to_dynamodb_type(FN_SAMPLE_NOTE)

        expected = '{"id": {"S": "f6a45e14-6c64-46cf-b23a-171accd84a15"}, "contentUrl": {"S": "http://localhost:8040/note/content"}, "createdTime": {"S": "2018-08-02T10:30:20.219Z"}, "modifiedTime": {"S": "2018-08-02T10:30:27.319Z"}, "tags": {"L": []}, "blocks": {"L": [{"M": {"type": {"S": "quill-delta"}, "tags": {"L": []}, "content": {"M": {"ops": {"L": [{"M": {"insert": {"S": "xyz\\n"}}}]}}}}}]}, "@context": {"S": "http://remotestorage.io/spec/modules/notes/note"}}'
        expected_type = json.loads(expected)

        self.assertDictEqual(dynamo_type, expected_type)

    def test_deserialize(self):
        dynamo_type = dynamocache._to_dynamodb_type(FN_SAMPLE_NOTE)
        dd = dynamocache._from_dynamodb_type(dynamo_type)

        self.assertDictEqual(dd, FN_SAMPLE_NOTE)

    def test_empty_string(self):
        tt = {"id":"c2c6ff53-ce43-4577-aac1-d4ea956a9863","contentUrl":"http://localhost:8040/note/content","createdTime":"2018-08-03T23:02:14.654Z","modifiedTime":"2018-08-03T23:02:14.654Z","tags":[],"blocks":[{"content":""}],"@context":"http://remotestorage.io/spec/modules/notes/note"}
        print(dynamocache._to_dynamodb_type(tt))

class TestReplaceDecimals(unittest.TestCase):
    def test(self):
        note = {
            'foo': 'bar',
            'cee': Decimal(3),
            'cee2': Decimal(3.5),
            'inner': [
                { 'a': Decimal(4) }
            ]
        }

        dynamocache.replace_decimals(note)

        json.dumps(note) # Ensure that it can be json serialized

        self.assertEqual(note['cee'], 3)
        self.assertTrue(isinstance(note['cee'], int))
        self.assertEqual(note['cee2'], 3.5)
        self.assertTrue(isinstance(note['cee2'], float))

if __name__ == '__main__':
    unittest.main()
