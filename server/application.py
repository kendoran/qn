#pylint: disable=C0301, C0111, E0401, C0103

import json

import flask
from flask import Flask, send_from_directory, make_response
from flask_compress import Compress

import dynamocache
import gdrive
import remotestorage as rs

from settings import SETTINGS
import fn_auth
from fn_auth import build_credentials, login_required
from fn_utils import fn_json_dumps, no_cache

application = Flask(__name__, static_folder=SETTINGS.STATIC_FOLDER)
Compress(application)

application.config.update(
    SECRET_KEY=SETTINGS.COOKIE_SECRET,
    SEND_FILE_MAX_AGE_DEFAULT=1700 # A bit less than the 1800 max age for js and css
)

application.register_blueprint(rs.api)
application.register_blueprint(fn_auth.api)

#============================
# Loading
#============================

# @application.route('/images/<path:path>')
# @login_required
# def images(path):
#   config = getConfig()
#   return send_from_directory(os.path.join(config.base_path, 'images'), path)

#============================
# Image uploading
#============================

@application.route("/token", methods=['GET'])
def token():
    return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZXJ2aWNlcyI6eyJja2VkaXRvci1jb2xsYWJvcmF0aW9uIjp7InBlcm1pc3Npb25zIjp7IioiOiJ3cml0ZSJ9fX0sInVzZXIiOnsiZW1haWwiOiJjb25sb3RpQGV4YW1wbGUuY29tIiwibmFtZSI6IkNsYXJlbmNlIERpYXoiLCJpZCI6IjRkYjVmMWFlLTgwNGMtNGViNi1hOWRlLWEyYjQyNDJiMjgyYiJ9LCJpc3MiOiJyYzFERnVGcEhxY1IzTWFoNnkwZSIsImp0aSI6IkZaX1RWVzR6cThIU1cyM0NKQnVxMVpiWXlfdjRySkJxIiwiaWF0IjoxNTI2OTM4NjE2fQ.JR90uCt2X_vTzP-xbBsugS654gPMc79JnYeYUwyq0Ok"

#============================
# Google Drive Sync
#============================

@application.route('/api/gdrive/active_file_id', methods=['GET'])
@no_cache
def get_active_file_id():
    return fn_json_dumps(gdrive.get_active_file_id(build_credentials()))

@application.route('/api/gdrive/active_file_contents', methods=['GET'])
@no_cache
def get_active_file_contents():
    credentials = build_credentials()
    file_id = gdrive.get_active_file_id(credentials)
    file_contents = gdrive.get_file_contents(credentials, file_id)

    return flask.Response(file_contents, 200, {}, 'application/json')

@application.route("/api/gdrive/list", methods=['GET'])
@login_required
@no_cache
def list_gdrive():
    credentials = build_credentials()
    result = gdrive.list_files(credentials)
    return flask.Response(fn_json_dumps(result, indent=2), 200, {}, 'application/json')

@application.route("/api/gdrive/list/images", methods=['GET'])
@login_required
@no_cache
def list_gdrive_images():
    credentials = build_credentials()
    result = gdrive.get_images_list(credentials)
    return flask.Response(fn_json_dumps(result, indent=2), 200, {}, 'application/json')

@application.route("/api/gdrive/save", methods=['GET']) # TODO: Should be a POST
@login_required
@no_cache
def save_to_gdrive():
    credentials = build_credentials()

    active_file_id = gdrive.get_active_file_id(credentials)
    gdrive_notes_raw = gdrive.get_file_contents(credentials, active_file_id)
    gdrive_parsed = json.loads(gdrive_notes_raw)
    gdrive_notes = dict((note['id'], note) for note in gdrive_parsed['notes'])

    dynamo_notes = dynamocache.get_notes_for_file(active_file_id)

    for note in dynamo_notes:
        status = note['fn-status'] if 'fn-status' in note else None

        if status == dynamocache.NOTE_STATUS_MODIFIED:
            del note['fn-status'] # Don't need to have 'modified' status in gdrive
            dynamocache.put_note(note)
            gdrive_notes[note['id']] = note
        if status == dynamocache.NOTE_STATUS_DELETED:
            del gdrive_notes[note['id']]

    file_data = {
        'version': 1,
        'notes': list(gdrive_notes.values())
    }

    gdrive.save_json(credentials, active_file_id, file_data)

    return flask.Response('Done', 200)

#============================
# Dynamo Cache
#============================

@application.route("/api/dc/file/<file_id>", methods=['GET'])
@login_required
def dc_get_notes_for_file(file_id):
    notes = dynamocache.get_notes_for_file(file_id)
    headers = {'Cache-Control': 'no-cache'}

    return flask.Response(fn_json_dumps(notes), 200, headers, "application/json")

@application.route("/api/dc/folder_description/<file_id>", methods=['GET'])
@login_required
def dc_get_folder_description_for_file(file_id):
    folder = dynamocache.get_folder_description_for_file(file_id)
    headers = {'Cache-Control': 'no-cache'}

    return flask.Response(fn_json_dumps(folder), 200, headers, "application/json")

@application.route("/api/dc/clear", methods=['GET'])
@login_required
def dc_clear():
    # TODO: Write to GDrive first?
    credentials = build_credentials()
    file_id = gdrive.get_active_file_id(credentials)

    notes = dynamocache.get_notes_for_file(file_id)
    for note in notes:
        dynamocache.delete_note_hard(note['id'])

    return flask.Response('Done', 200, {})

#============================
# Home page
#============================

@application.route('/')
@application.route('/index.html')
def index_html():
    return send_from_directory(application.static_folder, 'index.html')

# https://appcachefacts.info/
@application.route('/manifest.appcache')
@no_cache
def manifest_appcache():
    resp = make_response(send_from_directory(application.static_folder, 'manifest.appcache'))
    resp.headers['Content-Type'] = 'text/cache-manifest'
    return resp

# ============================
#  Init
# ============================

@application.errorhandler(401)
def error_401(e):
    return 'Error 401'

if __name__ == "__main__":
    application.run(host="127.0.0.1", port=5000, threaded=True)



