#pylint: disable=C0301, C0111, E0401, C0103

import functools

from authlib.client import OAuth2Session
import flask
import google.oauth2.credentials

import gdrive
from fn_utils import fn_json_dumps, no_cache
from settings import SETTINGS

ACCESS_TOKEN_URI = 'https://www.googleapis.com/oauth2/v4/token'
AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&prompt=consent'
AUTHORIZATION_SCOPE = 'openid email profile https://www.googleapis.com/auth/drive.file'

AUTH_TOKEN_KEY = 'auth_token'
AUTH_STATE_KEY = 'auth_state'
USER_INFO_KEY = 'user_info'

api = flask.Blueprint('fn_auth', __name__)

def is_logged_in():
    return True if AUTH_TOKEN_KEY in flask.session else False

def build_credentials():
    if not is_logged_in():
        raise Exception('User must be logged in')

    oauth2_tokens = flask.session[AUTH_TOKEN_KEY]
    return google.oauth2.credentials.Credentials(
        oauth2_tokens['access_token'],
        refresh_token=oauth2_tokens['refresh_token'],
        client_id=SETTINGS.CLIENT_ID,
        client_secret=SETTINGS.CLIENT_SECRET,
        token_uri=ACCESS_TOKEN_URI)

def login_required(func):
    @functools.wraps(func)
    def decorated_function(*args, **kws):
        if not is_logged_in():
            flask.abort(400, 'User must be logged in')
        return func(*args, **kws)
    return decorated_function

@api.route('/login')
@no_cache
def login():
    session = OAuth2Session(SETTINGS.CLIENT_ID, SETTINGS.CLIENT_SECRET, scope=AUTHORIZATION_SCOPE, redirect_uri=SETTINGS.AUTH_REDIRECT_URI)
    uri, state = session.authorization_url(AUTHORIZATION_URL)
    flask.session[AUTH_STATE_KEY] = state
    flask.session.permanent = True
    return flask.redirect(uri, code=302)

@api.route('/logout')
@no_cache
def logout():
    flask.session.pop(AUTH_TOKEN_KEY, None)
    flask.session.pop(AUTH_STATE_KEY, None)
    flask.session.pop(USER_INFO_KEY, None)

    return flask.redirect(SETTINGS.BASE_URI, code=302)

@api.route('/google/auth')
@no_cache
def google_auth_redirect():
    state = flask.request.args.get('state', default=None, type=None)
    if state:
        session = OAuth2Session(SETTINGS.CLIENT_ID, SETTINGS.CLIENT_SECRET, scope=AUTHORIZATION_SCOPE, state=state, redirect_uri=SETTINGS.AUTH_REDIRECT_URI)
        oauth2_tokens = session.fetch_access_token(ACCESS_TOKEN_URI, authorization_response=flask.request.url)
        flask.session[AUTH_TOKEN_KEY] = oauth2_tokens

        return flask.redirect(SETTINGS.BASE_URI, code=302)
    else:
        flask.abort(400, 'The "state" parameter is required on the URL query string')

@api.route('/user_info')
@no_cache
def user_info():
    if is_logged_in():
        credentials = build_credentials()
        result = gdrive.get_user_info(credentials)
    else:
        result = {'isSignedIn': False}

    return flask.Response(fn_json_dumps(result), 200, {}, 'application/json')
