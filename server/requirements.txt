authlib==0.7
boto3
dynamodb_encryption_sdk
flask
flask-compress
google-api-python-client
google-auth
werkzeug
