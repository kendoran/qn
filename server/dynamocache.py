#pylint: disable=C0301, C0111, C0103
from decimal import Decimal

import boto3
from boto3.dynamodb.types import TypeDeserializer, TypeSerializer

from settings import Settings

NOTE_STATUS_MODIFIED = 'modified'
NOTE_STATUS_DELETED = 'deleted'

SETTINGS = Settings()

client = boto3.client(
    'dynamodb',
    aws_access_key_id=SETTINGS.AWS_ACCESS_KEY,
    aws_secret_access_key=SETTINGS.AWS_SECRET_KEY,
    region_name=SETTINGS.AWS_REGION
)

# Decimal types are not supported by the JSON serializer,
# So can convert back from Decimal to float/int here.
def replace_decimals(d):
    if isinstance(d, dict):
        for k, v in d.items():
            if isinstance(v, dict):
                replace_decimals(v)
            if isinstance(v, list):
                for i in v:
                    replace_decimals(i)
            elif isinstance(v, Decimal):
                if v % 1 == 0:
                    d[k] = int(v)
                else:
                    d[k] = float(v)

def _remove_empty(d):
    if isinstance(d, dict):
        empty_strings = []

        for k, v in d.items():
            if isinstance(v, dict):
                _remove_empty(v)
            if isinstance(v, list):
                for i in v:
                    _remove_empty(i)
            if isinstance(v, float):
                d[k] = Decimal(v) # float types are not supported by boto3 https://github.com/boto/boto3/issues/665#issue-157775588
            elif v == '':
                empty_strings.append(k)

        for k in empty_strings:
            d[k] = None

def _to_dynamodb_type(dct):
    _remove_empty(dct)
    result_ = TypeSerializer().serialize(dct)
    return result_['M']

def _from_dynamodb_type(dct):
    if isinstance(dct, dict):
        dct = {'M' : dct}
    elif isinstance(dct, list):
        dct = {'L' : dct}

    obj = TypeDeserializer().deserialize(dct)
    replace_decimals(obj)
    return obj

def get_note(note_id):
    """
    Queries DynamoDB for a note with id matching note_id

    Returns the note in the the internal note format,
    or None if the note doesn't exist in DynamoDB.
    """
    # https://boto3.readthedocs.io/en/latest/reference/services/dynamodb.html#DynamoDB.Client.get_item
    item = client.get_item(
        TableName=SETTINGS.DYNAMO_CACHE_TABLE_NAME,
        Key={
            'id': {'S': note_id}
        }
    )

    return _from_dynamodb_type(item['Item']) if 'Item' in item else None

def put_note(note):
    # https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/dynamodb.html#DynamoDB.Client.put_item
    client.put_item(
        TableName=SETTINGS.DYNAMO_CACHE_TABLE_NAME,
        Item=_to_dynamodb_type(note))

    return None

def delete_note_hard(note_id):
    client.delete_item(
        TableName=SETTINGS.DYNAMO_CACHE_TABLE_NAME,
        Key={
            'id': {'S': note_id}
        }
    )

def get_notes_for_file(file_id):
    """
    Queries DynamoDB for the notes with fileId matching file_id

    Returns the list of notes in the the internal note format,
    or an empty list if the file hasn't been loaded into DynamoDB.
    """
    query_result = client.query(
        TableName=SETTINGS.DYNAMO_CACHE_TABLE_NAME,
        IndexName=SETTINGS.DYNAMO_FILE_ID_INDEX_NAME,
        ExpressionAttributeValues={
            ':v1': {'S': file_id},
        },
        KeyConditionExpression='fileId = :v1',
    )

    result = []

    for item in query_result['Items']:
        result.append(_from_dynamodb_type(item))

    return result

def get_folder_description_for_file(file_id):
    """
    Queries DynamoDB for the notes with fileId matching file_id

    Returns the list of notes in the the internal note format,
    or an empty list if the file hasn't been loaded into DynamoDB.
    """
    query_result = client.query(
        TableName=SETTINGS.DYNAMO_CACHE_TABLE_NAME,
        IndexName=SETTINGS.DYNAMO_FILE_ID_INDEX_NAME,
        ExpressionAttributeValues={
            ':v1': {'S': file_id},
        },
        KeyConditionExpression='fileId = :v1',
        ProjectionExpression="id, etag, modifiedTime",
    )

    result = []

    for item in query_result['Items']:
        result.append(_from_dynamodb_type(item))

    return result


# from dynamodb_encryption_sdk.encrypted.client import EncryptedClient
# from dynamodb_encryption_sdk.identifiers import CryptoAction
# from dynamodb_encryption_sdk.material_providers.aws_kms import AwsKmsCryptographicMaterialsProvider
# from dynamodb_encryption_sdk.structures import AttributeActions

# def build_encrypted_client():
#     return EncryptedClient(
#         attribute_actions=AttributeActions(
#             default_action=CryptoAction.DO_NOTHING,
#             attribute_actions={
#                 'blocks': CryptoAction.ENCRYPT_AND_SIGN,
#             }
#         ),
#         client=client,
#         materials_provider=AwsKmsCryptographicMaterialsProvider(key_id=SETTINGS.AWS_CMK_ID)
#     )