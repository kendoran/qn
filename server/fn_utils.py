#pylint: disable=C0301, C0111, C0103

from decimal import Decimal
import functools
import json
import sys

import flask

def print_to_console(text):
    print(text, file=sys.stdout)

def fn_json_default(obj):
    if isinstance(obj, Decimal):
        return float(obj)
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)

def fn_json_dumps(obj, indent=None):
    return json.dumps(obj, default=fn_json_default, indent=indent)

def no_cache(view):
    @functools.wraps(view)
    def no_cache_impl(*args, **kwargs):
        response = flask.make_response(view(*args, **kwargs))
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    return functools.update_wrapper(no_cache_impl, view)