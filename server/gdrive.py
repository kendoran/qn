#pylint: disable=C0301, C0111, E0401, E1101, C0103

import io

# https://google.github.io/google-api-python-client/docs/epy/googleapiclient.discovery-module.html
# https://developers.google.com/resources/api-libraries/documentation/drive/v3/python/latest/

from apiclient.http import MediaIoBaseDownload, MediaIoBaseUpload
from googleapiclient.discovery import build
import flask

from fn_utils import print_to_console, fn_json_dumps
from settings import Settings

SETTINGS = Settings()

DEFAULT_FILE_CONTENTS = {
    'version': 1,
    'notes:': []
}

GDRIVE_ACTIVE_FILE_ID_KEY = 'gdrive_active_file_id'

def generate_id(files_api):
    """Returns a singular id which can be used to create a google drive file"""
    return files_api.generateIds(count=1)['ids'][0]

def _create_active_file(files_api, folder_id):
    generate_ids_result = files_api.generateIds(count=1).execute()
    file_id = generate_ids_result['ids'][0]

    contents = DEFAULT_FILE_CONTENTS
    contents['id'] = file_id

    body = {
        'id': file_id,
        'name': 'fn-' + file_id + '.json',
        'mimeType': 'application/json',
        'parents': [folder_id]
    }

    media_body = MediaIoBaseUpload(
        io.BytesIO(fn_json_dumps(contents).encode()),
        mimetype='application/json; charset=UTF-8',
        resumable=True)

    # TODO: Assert file was created, and that there was no failure
    files_api.create(
        body=body,
        media_body=media_body,
        fields='id,name,mimeType,createdTime,modifiedTime').execute()

    print_to_console("Created new json file with name: " + body['name'])

    return file_id

def _get_fn_folder_id(files_api):
    search_result = files_api.list(pageSize=10, orderBy="folder", q='trashed=false and mimeType="application/vnd.google-apps.folder" and name="'+ SETTINGS.FOLDER_NAME + '"').execute()

    if search_result['files']:
        folder_id = search_result['files'][0]['id']
    else:
        folder_metadata = {
            'name': SETTINGS.FOLDER_NAME,
            'mimeType': 'application/vnd.google-apps.folder'
        }

        folder = files_api.create(body=folder_metadata, fields='id').execute()
        folder_id = folder.get('id')

    return folder_id

def get_active_file_id(credentials):
    if GDRIVE_ACTIVE_FILE_ID_KEY in flask.session:
        return flask.session[GDRIVE_ACTIVE_FILE_ID_KEY]

    files_api = build('drive', 'v3', credentials=credentials).files()
    folder_id = _get_fn_folder_id(files_api)

    list_response = files_api.list(
        fields="files(id,name,mimeType,properties,appProperties,createdTime,modifiedTime)",
        orderBy="createdTime desc",
        q="'" + folder_id +  "' in parents and trashed=false").execute()

    for file in list_response['files']:
        if file['mimeType'] == 'application/json': # take the first json file. TODO: support multiple json files
            file_id = file['id']
            flask.session[GDRIVE_ACTIVE_FILE_ID_KEY] = file_id
            return file_id

    file_id = _create_active_file(files_api, folder_id)
    flask.session[GDRIVE_ACTIVE_FILE_ID_KEY] = file_id
    return file_id

def get_file_contents(credentials, file_id):
    files_api = build('drive', 'v3', credentials=credentials).files()
    return files_api.get_media(fileId=file_id).execute()

def get_user_info(credentials):
    # https://stackoverflow.com/a/26462143
    oauth2_client = build('oauth2', 'v2', credentials=credentials)
    user_info = oauth2_client.userinfo().get().execute()

    return {
        'id': user_info['id'],
        'isSignedIn': True,
        'email': user_info['email'],
        'name': user_info['name'],
        'givenName': user_info['given_name'],
        'familyName': user_info['family_name'],
        'picture': user_info['picture'],
        'locale': user_info['locale']
    }

def save_json(credentials, file_id, file_data):
    files_api = build('drive', 'v3', credentials=credentials).files()

    media_body = MediaIoBaseUpload(io.BytesIO(fn_json_dumps(file_data).encode()),
                                   mimetype='application/json; charset=UTF-8',
                                   resumable=True)
                             
    return files_api.update(fileId=file_id,
                            media_body=media_body,
                            fields='id,name,mimeType,createdTime,modifiedTime').execute()

def get_images_list(credentials):
    files_api = build('drive', 'v3', credentials=credentials).files()

    folder_id = _get_fn_folder_id(files_api)

    list_response = files_api.list(
        fields="files(id,name,mimeType,properties,appProperties,createdTime,modifiedTime,size,md5Checksum)",
        orderBy="createdTime desc",
        q="'" + folder_id +  "' in parents and trashed=false").execute()

    files = list_response['files']

    return list(filter(lambda x: x['mimeType'] == 'image/jpeg' or x['mimeType'] == 'image/png', files))

def get_image_metadata(credentials, file_id):
    files_api = build('drive', 'v3', credentials=credentials).files()
    request = files_api.get(
        fields="id,name,mimeType,properties,appProperties,createdTime,modifiedTime,size,md5Checksum",
        fileId=file_id)

    return request.execute()

def get_image_stream(credentials, file_id):
    files_api = build('drive', 'v3', credentials=credentials).files()
    request = files_api.get_media(fileId=file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)

    done = False
    while done is False:
        status, done = downloader.next_chunk()

    fh.seek(0)
    return fh

def save_image(credentials, file_name, mime_type, file_data):
    files_api = build('drive', 'v3', credentials=credentials).files()
    generate_ids_result = files_api.generateIds(count=1).execute()
    file_id = generate_ids_result['ids'][0]

    folder_id = _get_fn_folder_id(files_api)

    body = {
        'id': file_id,
        'name': file_name,
        'mimeType': mime_type,
        'parents': [folder_id]
    }

    media_body = MediaIoBaseUpload(file_data,
                                   mimetype=mime_type,
                                   resumable=True)

    files_api.create(body=body,
                     media_body=media_body,
                     fields='id,name,mimeType,createdTime,modifiedTime').execute()

    return file_id

def list_files(credentials):
    files_api = build('drive', 'v3', credentials=credentials).files()
    return files_api.list(pageSize=10, orderBy="folder", q='trashed=false', fields='files(id,name,mimeType,createdTime,modifiedTime,shared,webContentLink)').execute()